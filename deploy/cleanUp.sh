#!/bin/sh

echo "clean up files older than 30 days under back up folder"
find /home/omega/backups -type f -mtime +30 -exec rm -f {} \;