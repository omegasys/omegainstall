##################################################
# deploy.sh used by jenkins
#
# Release Note:
# Version 4.8.4
# Improvements:
	• 3rd parameter added to deploy.sh. It is used to differentiate different server environments. For e.g. disable tomcat start afterward on    
          Demo servers since it has tomcat auto start set up. 
	• Refactored tomcat start, kill to be server based. 
	• Fixed tomcat kill, start, Working as expected. 

##################################################
# 
# Usage:

# Demo servers has auto tomcat start, no need to restart tomcat again. 

# Fulfill this purpose by passing a 3rd parameter. 

# sh ~/bin/schedule-deploy.sh 4.80 'cms' n

# Server without tomcat auto start
# passing null into the 3rd parameter will work.

# bin/schedule-deploy.sh 4.80 'core4 ics cms'

# Push Script users:
# Now you can replace several duplicate deploy blocks with:

# e.g.:
# $SSH_USERNAME@$SSH_HOST "bin/schedule-deploy.sh 4.80 'core4 ics cms'"

