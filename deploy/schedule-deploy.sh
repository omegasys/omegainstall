#!/usr/bin/env bash
#set -e

TODAYTIME="$(date +%F)-$(date +%T)"
DATE_EXT="$(date) +%Y_%m_%d_%H_%M_%S"
RED='\033[0;31m'
GREEN='\033[0;32m'
APPLICATION_HOME="/home/omega/apps/"
UPLOAD="/home/omega/uploads"
BACKUP="/home/omega/uploads/backup"
CATALINA=/home/omega/tomcat/bin/catalina.sh

echo $PWD
echo $USER

rm -rf ~/bin/deploy*.sh
cp install/apps/bin/deploy.sh ~/bin/
chmod +x  ~/bin/deploy.sh

if test -z "$1"
then
        echo "You must specify release version to deploy"
        exit
fi

# get release version from first input argument
BRANCH=$1

if test -z "$2"
then
        echo "You must specify applications to deploy"
        exit
fi

APPS=$2

echo "Applications to be deployed are:"
echo $APPS

echo "Stopping tomcat"
pkill -9 -f tomcat

echo "create back up folder if not exist"
mkdir -p "$BACKUP"

for NAME in $APPS
do
  case "$NAME" in
    'core4')
        echo "********** Deploy ""$NAME"" Start**********"
        echo ${NAME}
        APP="OmegaCore"

        if [[ -f "$UPLOAD/core.tar.gz" ]]
        then
            echo "****************Backup****************"
            rsync $UPLOAD/core.tar.gz $BACKUP/core.tar.gz_$TODAYTIME
        fi

        TARGET_WAR=core.tar.gz
        echo "Deploying File"  $TARGET_WAR
        #Upload the war files
        echo "Uploading War File"  $TARGET_WAR
        rsync core.tar.gz $UPLOAD/$TARGET_WAR
        echo "****** Deploying ****** ""$NAME"
        sh ~/bin/deploy.sh
        # for Core4, version is acquired from git
        echo "$APP Upgraded to $GIT_COMMIT";;
    'ics'|'cms'|'connect'|'ps'|'ips'|'txs'|'tron')
        echo "Deploy ""$NAME"" Start**********"
        echo ${NAME}
        APP="Omega""${NAME^}"

        VERSION_FILE_LOCATION="/home/omega/apps/$APP/WEB-INF/classes/version.txt"

        mkdir -p "$APPLICATION_HOME/$APP"
        #Check Version Prior to the deployment
        if [[ -d "$APPLICATION_HOME/$APP" ]]
              then
              PRE_VERSION=$(awk {print} $VERSION_FILE_LOCATION)
        else
            PRE_VERSION=N/A
        fi
        echo PRE_VERSION=$PRE_VERSION

        if [ "$NAME" == "ics" ]; then
            APPLICATION="OmegaIcsApps"
            TARGET_WAR=$APPLICATION-$BRANCH.war
        else
            TARGET_WAR=$APP-$BRANCH.war
        fi

        if [[ -f "$UPLOAD/$TARGET_WAR" ]]
        then
            echo "****************Backup****************"
            rsync $UPLOAD/$TARGET_WAR $BACKUP/$TARGET_WAR"-"$TODAYTIME
        fi

        echo "Deploying File"  $TARGET_WAR
        #Upload the war files
        echo "Uploading War File"  $TARGET_WAR
        rsync $TARGET_WAR $UPLOAD/$TARGET_WAR

        echo " ****** deploying ****** ""$NAME"

        sh ~/bin/deploy.sh $UPLOAD/$TARGET_WAR

        VERSION=$(awk {print} $VERSION_FILE_LOCATION)
        rc=$?
        echo "$NAME" " Deployment Status ""$rc"
        echo "$APP Upgraded from $PRE_VERSION to $VERSION"
        echo "*****************" "$APP" "deploy done ****************************";;
  esac
done

if [ ! -z $3 ]
then
    : # demo env no tomcat start needed
else
    : #
    echo "Restart tomcat No default tomcat auto start setting on this server"
    sleep 2
    $CATALINA start
fi

echo "clean up files older than 30 days under back up folder"
find /home/omega/backups -type f -mtime +30 -exec rm -f {} \;
