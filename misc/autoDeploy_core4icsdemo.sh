#!/bin/sh

DOWNLOAD_DIR="/Users/jcheung/Downloads"
SSH_HOST="10.0.1.54"
SSH_USERNAME="omega"
SSH_PASSWORD="!0m3ga2016"
TODAYTIME="$(date +%F)-$(date +%T)"


VERSION=$(find . -maxdepth 1 -name "Omega*.war" | sed -e 's/.*Apps-\(.*\).war/\1/')
if [ $(sshpass -p $SSH_PASSWORD ssh -oStrictHostKeyChecking=no $SSH_USERNAME@$SSH_HOST "cd uploads; find . -maxdepth 1 -name core.tar.gz | wc -l") = 1 ]; then
	if [ $(sshpass -p $SSH_PASSWORD ssh -oStrictHostKeyChecking=no $SSH_USERNAME@$SSH_HOST "cd uploads; find . -maxdepth 1 -name OmegaIcsApps-${VERSION}.war | wc -l") = 1 ]; then
		sshpass -p $SSH_PASSWORD ssh -oStrictHostKeyChecking=no $SSH_USERNAME@$SSH_HOST "cd uploads; mv OmegaIcsApps-${VERSION}.war OmegaIcsApps-${VERSION}.war.${TODAYTIME}; mv core.tar.gz core.tar.gz.${TODAYTIME}"
		sshpass -p $SSH_PASSWORD rsync -a --progress "OmegaIcsApps-${VERSION}.war" "${SSH_USERNAME}@${SSH_HOST}:uploads/OmegaIcsApps-${VERSION}.war"
		sshpass -p $SSH_PASSWORD rsync -a --progress "core.tar.gz" "${SSH_USERNAME}@${SSH_HOST}:uploads/core.tar.gz"
		sshpass -p $SSH_PASSWORD ssh -oStrictHostKeyChecking=no $SSH_USERNAME@$SSH_HOST "cd bin; ./deployCore.sh /home/omega/uploads/core.tar.gz"
		sleep 15
		sshpass -p $SSH_PASSWORD ssh -oStrictHostKeyChecking=no $SSH_USERNAME@$SSH_HOST "cd bin; ./deployIcs.sh /home/omega/uploads/OmegaIcsApps-${VERSION}.war"				
		echo "done!"
	else
		echo "cannot find OmegaIcsApps-${VERSION}.war in ${SSH_USERNAME}@${SSH_HOST}:uploads"
	fi
else
	echo "cannot find core.tar.gz in ${SSH_USERNAME}@${SSH_HOST}:uploads"
fi


