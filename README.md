# Setup Omega Servers and Applications
This document explains how to use OmegaInstall to setup Omega Servers and Applications. 

If you are looking for legacy 
document, check here [Legacy Setup OmegaApplications on new servers](https://omegasys.atlassian.net/wiki/spaces/WS/pages/86704180/Setup+OmegaApplications+on+new+servers).

**You are using master version that support 4.80/Java 21. For version prior to that, please check in the following version.**

      git checkout pre-Java21

## Step 1 Connect to the server

Before we can do anything, we need to get into the server via either `SSH` or `Remote Desktop`

Login Credentials should be found in corresponding confluence wiki pages

## Step 2 Install essential tools

We need these tools to perform other setup steps, simply run the following command after you've connected to the server. 

    sudo yum install git wget telnet -y


## Step 3 Setup database server(s)

Because other applications depend on Database IP and schema name to boot up, we need to initialize
database server first. 

A complete guide created by Mak can be found in [DB Migration Process](https://omegasys.atlassian.net/wiki/spaces/OMGDB/pages/93585415/DB+Migration+Process).

After setting up DB server, we want to make sure it is reachable in our application servers.
Run the following commands on any of the application servers to test DB connection.

    telnet <db ip address> 1433

We should get the following response if DB is setup properly. Use Ctrl-C to get out of it.

    -bash-4.2$ telnet <db ip address> 1433
    Trying <db ip address>...
    Connected to <db ip address>
    Escape character is '^]'.
    
## Step 4 Setup application servers

We always follow the following sub-steps to setup any of the application servers (Core4/Cms/Ps/Tron) 

1. #### Check out OmegaInstall
       
        git clone https://bitbucket.org/omegasys/omegainstall.git OmegaInstall
    
2. #### Run check-user script (Conditional, root)

If we don't have linux server login user `omega`, we need to run the following command with `root` access.

        ~/OmegaInstall/install/apps/bin/checkUser.sh
       
3. #### Run setup-server script

      #### 3.1 For Tomcat Apps
   Get Java from Jenkins or S3

            cd /home/omega/uploads
            wget --user=omegasys --password=11da2754ccd5311cbf0ad7690b35b2c13e --auth-no-challenge https://jenkins.omegasys.eu/userContent/JDK/java-21-amazon-corretto-devel-21.0.4.7-1.x86_64.rpm


   After wget, you can proceed.

        . ~/OmegaInstall/install/scripts/setup-server.sh <app name>
        
         `<app name> can be core/ps/txs,  e.g. . ~/OmegaInstall/install/scripts/setup-server.sh core`  
      
         If the following prompt appears, we need to type the DB server information we just configured in 
         Step 3, in the format of `<db ip address>:<db port>/<db name>`
      
           Please provide database connection string, e.g. 10.0.1.1:1433/omega_demo
       
         After the following complete message, please type `exit` and then reconnect to the server.
   
           Server Setup Complete!
           Please close the session and login again BEFORE installing applications
   #### 3.2 For embedded Tomcat Apps
      This will setup java and apps/conf directory. However, for Rpt, use https://omegasys.atlassian.net/wiki/spaces/CU/pages/2980642922/OmegaRPT+Deployment

            . ~/OmegaInstall/install/scripts/setup-embedded-tomcat-server.sh <app name>


4. #### Run install script

        . ~/OmegaInstall/install/scripts/install.sh
       
      Follow the prompt and select which application(s) you want to install
      
      ![install_prompt](install/images/install_prompt.png)
      
5. #### Download package(s) using `wget`
      
      Assuming we have already build the desired package(s) and uploaded to dropbox, we will need to 
      download the package(s) to the server. Use the following command.
      
        cd ~/uploads/
        wget https://www.dropbox.com/s/SOME-HASH-CODE/OmegaXXX-VERSION_NUMBER.war
        
6. #### Install deployment scripts       
    sh ~/OmegaInstall/install/scripts/install-deployment-scripts.sh

7. #### Deploy application(s)

    Each package needs to be deployed individually.
    git pull under OmegaInstall to get updates
    cp install/apps/bin/deploy.sh ~/bin/

* Core: `sh ~/bin/deploy.sh`
* Others: `sh bin/deploy.sh ~/uploads/OmegaTron-4.8.4.war`


## Step 5 Config Firewall (optional)

If tomcat is running but the browser shows a blank page, on the other hand, when we call `curl <server ip>:8081` in the terminal, it returns a HTML page.

That means the application has successfully started, but the firewall on the server blocked the page.
To change the firewall setting, run the following commands.

    sudo sh ~/OmegaInstall/install/apps/bin/checkFirewall.sh
    
In case of `firewall-cmd: command not found`

    sudo yum install firewalld
    sudo systemctl start firewalld
    sudo systemctl enable firewalld
    
## Step 6 Verify all applications are running

System Report Links

* CMS: `<server ip>:8081/j/Login.action`
* PS : `<server ip>:8081/ps/ips/login`
* Connect: `<server ip>:8081/connect/SystemStatus.action`
* Tron: `<server ip>:8081/omegatron/spr/SystemStatus.action`

Swagger Links

* ICS: `<server ip>:8081/ics/swagger-ui.html`
* PS2/IPS: `<server ip>:8081/ips/swagger-ui.html`
* Tron2/TXS: `<server ip>:8081/omegatxs/swagger-ui.html`

Scripts available for validating git version:  
     
    ~/bin/displayVersion.sh

## How to update DB connection string after application installation

On rare cases, DB connection string may change after we have set up all applications.
We can use the following steps to update DB information

First, we need to stop tomcat

    ~/tomcat/bin/catalina.sh stop
    
Then run this script    

    . ~/bin/updateDBConnection.sh
    
Check it

    echo $DBCONNECTION 
    
When the prompt appears, we can provide new DB information in the format of `<db ip address>:<db port>/<db name>`.
Then re-run install.sh and select which application(s) need to be updated

    . ~/OmegaInstall/install/scripts/install.sh

Finally, restart tomcat service

    ~/tomcat/bin/catalina.sh start

## If you installed wrong application

If you accidentally installed wrong application, you can run this script to clean-up all applications
and start over again.

    . ~/OmegaInstall/install/scripts/clean-all.sh
    
## If Java version mismatch

If you type `java -version` and it shows something like `OpenJdk version xxxx`, then you need to do the following steps

    ls -ln /usr/bin/java
You might get a result such as:
    
    lrwxrwxrwx. 1 root root 22 Aug  5 17:01 /usr/bin/java -> /etc/alternatives/java
    
Which would mean you can have several java versions on your system and use alternatives to change the default one. 
Since this is a server where we want to make sure only one version of Java is installed.
First, you need to remove the existing symbolic link

    sudo rm /usr/bin/java
    
Secondly, create a new symbolic link

    ln -s /home/omega/java/jre/bin/java /usr/bin/java

Finally, use `java -version` again and verify the java version number
