
###############################################################
################################################################
## Replace $PASSWORD and DB connection string in config map

kubectl create namespace omega

# create config map

kubectl --namespace=omega create -f omega-config.yml

kubectl --namespace=omega create -f omega-{apps}-k8s-deployment.yaml

###### Quick commands
###################################################################

kubectl get pods --namespace=omega

kubectl --namespace=omega delete deployment --all
kubectl exec -it omega-core4-c5c79f644-4g8rs bash --namespace=omega
kubectl top nodes

kubectl describe deployment omega-cms
kubectl describe pod --namespace=omega omega-txs-798ff65448-hs5qd 
kubectl logs --namespace=omega omega-core4-bbd56d6d4-xc7mk --tail=100

## shut down tomcat for all
kubectl --namespace=omega delete deployment --all
 
## delete and restart app
kubectl delete pod <<pod name>>

## delete and restart CMS
kubectl delete pod -l app=omega-cms

## delete and restart all the applications
kubectl delete pod -l type=app