
###############################################################
################################################################
# Step 1 Replace $PASSWORD and DB connection string to your own in config map
minikube start --cpus 4 --memory 8192 --logtostderr --v=3
minikube dashboard

kubectl create namespace omega

kubectl --namespace=omega create -f omega-config.yml
kubectl --namespace=omega create -f omega-{apps}-k8s-deployment.yaml
# for example:
kubectl --namespace=omega create -f omega-cms-k8s-deployment.yaml
kubectl --namespace=omega create -f omega-core4-k8s-deployment.yaml

###### Quick commands
###################################################################

kubectl get pods --namespace=omega

kubectl --namespace=omega get po -o wide
#NAME                      READY     STATUS    RESTARTS   AGE       IP           NODE
#omega-cms-6bd59ccd66-4zwh9   1/1       Running   0          6m        172.17.0.4   minikube

kubectl --namespace=omega describe deployment omega-core4
kubectl --namespace=omega describe deployment omega-cms
kubectl describe pod --namespace=omega omega-core4-7969b4f65b-7r2t5
kubectl describe pod --namespace=omega omega-txs-798ff65448-hs5qd 
#omega-core4-6bff7c5476-hjfbb acquried from get pods
kubectl logs --namespace=omega omega-core4-6bff7c5476-hjfbb --tail=100

kubectl --namespace=omega exec -it omega-core4-7969b4f65b-7r2t5 bash 
curl -I 172.17.0.5:8081/ics/home/performance
# 403 
###### Manual probe test
###################################################################
kubectl --namespace=omega exec -t omega-cms-6bd59ccd66-4zwh9 -- curl -I 172.17.0.5

# if 200 response, the path is configured properly and the readiness probe
# should be passing.

kubectl --namespace=omega exec -t omega-core4-7dff5f5f9-9ht5m -- curl -I 172.17.0.5:8081/j/Login.action
kubectl --namespace=omega exec -t omega-cms-6bd59ccd66-4zwh9 -- curl -I 172.17.0.4:8081
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                            Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
HTTP/1.1 200 OK
Server: Apache-Coyote/1.1
Content-Type: text/html;charset=UTF-8
Transfer-Encoding: chunked
Vary: Accept-Encoding
Date: Tue, 25 Jun 2019 21:23:13 GMT

## shut down tomcat for all
kubectl --namespace=omega delete deployment --all
kubectl --namespace=omega delete service --all
kubectl --namespace=omega delete configmap --all
kubectl top nodes

## delete and restart app
kubectl delete pod <<pod name>>

## delete and restart CMS
kubectl delete pod -l app=omega-cms

## delete and restart all the applications
kubectl delete pod -l type=app