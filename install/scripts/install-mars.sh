#!/bin/bash

echo "*******************************************"
echo "Installing Omega Mars"
echo "*******************************************"

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! -d "/home/omega/apps/OmegaMars" ]; then
  echo "create apps/OmegaMars dir"
  mkdir /home/omega/apps/OmegaMars
fi

echo "copy mars.xml"
yes | cp -rf /home/omega/OmegaInstall/install/apps/mars.xml /home/omega/tomcat/conf/Catalina/localhost/

echo "create apps/conf dir"
if [ ! -d "/home/omega/apps/conf/" ]; then
mkdir /home/omega/apps/conf/
fi

echo "set OmegaMars.conf"
if [ -e "/home/omega/apps/conf/OmegaMars.conf" ]; then
rm /home/omega/apps/conf/OmegaMars.conf
fi
echo "jdbcUrl=jdbc:jtds:sqlserver://"$DBCONNECTION";useLOBs=false" >> /home/omega/apps/conf/OmegaMars.conf
echo "user=admin_all
password=admin_all
" >> /home/omega/apps/conf/OmegaMars.conf

echo "copy mars_logback"
yes | cp -rf /home/omega/OmegaInstall/install/apps/conf/mars_logback.properties /home/omega/apps/conf/

echo "copy deploy script"
yes | cp -rf /home/omega/OmegaInstall/install/apps/bin/deployMars.sh /home/omega/bin/
chmod +x /home/oemga/bin/deployMars.sh

echo "OmegaMars Installed"
