#!/bin/bash
echo "##################################################################################################################################"
echo "# Starting from Java 21, we download the rpm from https://docs.aws.amazon.com/corretto/latest/corretto-21-ug/downloads-list.html #"
echo "# It seems only version is able to work for all Linux. But so I test it in Redhat                                                #"
printf "##################################################################################################################################\n"
#64 bit
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CODE_DIR=$SCRIPTS_DIR/../code
JAVA_DIR=$SCRIPTS_DIR/../../../java
LINUX_VER=$(awk -F'=' '/VERSION_ID/{ gsub(/"/,""); print $2}' /etc/os-release)
LINUX_NAME=$(awk -F'=' '/^NAME/{ gsub(/"/,""); print $2}' /etc/os-release)
echo "LINUX_NAME: $LINUX_NAME"
echo "LINUX_VER: $LINUX_VER"
echo "---------------------------------------------------------------------------------------------------------------------"

JDK="java-21-amazon-corretto-devel-21.0.4.7-1.x86_64.rpm"

echo "CODE_DIR:$CODE_DIR, JDK:$JDK"

#Unzip the jdk to the right folder
cd $CODE_DIR

echo "Start sudo yum localinstall"

UPLOAD_DIR="/home/omega/uploads"
# Check if the directory exists
if [ ! -d "$UPLOAD_DIR" ]; then
    echo "Directory $DIR does not exist. Creating it..."
    mkdir -p "$UPLOAD_DIR"
    if [ $? -eq 0 ]; then
        echo "Directory $UPLOAD_DIR created successfully."
    else
        echo "Failed to create directory $UPLOAD_DIR."
    fi
else
    echo "Directory $UPLOAD_DIR already exists."
fi

# Check if the JDK file exists
if [ ! -f "/home/omega/uploads/$JDK" ]; then
    echo "$JDK does not exist in /home/omega/uploads"
    cd "/home/omega/uploads"
    echo "Downloading the JDK from Jenkins"
    wget --user=omegasys --password=11da2754ccd5311cbf0ad7690b35b2c13e --auth-no-challenge https://jenkins.omegasys.eu/userContent/JDK/java-21-amazon-corretto-devel-21.0.4.7-1.x86_64.rpm
    echo "Download completed"
    cd ..
else
    echo "$JDK already exists in /home/omega/uploads"
fi

sudo yum localinstall /home/omega/uploads/$JDK -y
echo "-sf $JAVA_DIR /etc/alternatives/java_sdk_21_openjdk"
sudo ln -sf /usr/lib/jvm/java-21-amazon-corretto /etc/alternatives/java_sdk_21_openjdk

#There is 2 Java ( /usr/bin/java and /home/omega/java). So set them up below
sudo unlink $JAVA_DIR 2>/dev/null
sudo ln -sf /etc/alternatives/java_sdk_21_openjdk $JAVA_DIR
sudo unlink /etc/alternatives/java 2>/dev/null
sudo ln -sf /etc/alternatives/java_sdk_21_openjdk/bin/java /etc/alternatives/java

#setup bash_profile
rm ~/.bash_profile
echo 'if [ -f ~/.bashrc ];' >> ~/.bash_profile \
&& echo 'then . ~/.bashrc  ' >> ~/.bash_profile \
&& echo 'fi' >> ~/.bash_profile \
&& echo 'JAVA_HOME=/home/omega/java' >> ~/.bash_profile \
&& echo 'PATH=$PATH:$HOME/.local/bin:$HOME/bin:$JAVA_HOME/bin' >> ~/.bash_profile \
&& echo 'export PATH'>> ~/.bash_profile && source ~/.bash_profile
source ~/.bash_profile

#We don't need tp copy keystore file. The default cacerts from JDK should be good enough
#cp -rf $CODE_DIR/cacerts $JAVA_HOME/jre/lib/security/.

#cat >>~/.bash_profile<<EOF
#alias ..="cd .." ...="cd ../.." ....="cd ../../.."
#alias gitsl="git shortlog"
#alias gitfc="git commit --amend"
#alias clearh="cat /dev/null > ~/.bash_history && history -c"
#alias lsbyM="ls -lt --block-size=M"
#alias d.="find . -name '*.DS_Store' -type f -delete"
#alias ts="~/tomcat/bin/catalina.sh stop"
#alias tjpda="~/tomcat/bin/catalina.sh jpda start"
#alias tjpdastop="~/tomcat/bin/catalina.sh jpda stop"
#alias tr="~/tomcat/bin/catalina.sh run"
#alias gt="ps -ef | grep tomcat"
#alias tf="tail -f ~/tomcat/logs/catalina.out"
#alias mvnps="mvn clean install -DskipTests -pl OmegaPs -am"
#alias gs="ps aux | grep ssh"
#EOF


java -version
