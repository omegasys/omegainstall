#!/bin/bash

echo "*******************************************"
echo "Installing Omega PS2/Ips"
echo "*******************************************"

# deploy code
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! -d "/home/omega/apps/OmegaIps" ]; then
mkdir /home/omega/apps/OmegaIps
fi

yes | cp -rf /home/omega/OmegaInstall/install/apps/ips.xml /home/omega/tomcat/conf/Catalina/localhost

if [ ! -d "/home/omega/apps/conf/" ]; then
mkdir /home/omega/apps/conf/
fi

if [ -e "/home/omega/apps/conf/OmegaIps.conf" ]; then
rm /home/omega/apps/conf/OmegaIps.conf
fi
echo "jdbcUrl=jdbc:jtds:sqlserver://"$DBCONNECTION >> /home/omega/apps/conf/OmegaIps.conf
echo "user=admin_all
password=admin_all
" >> /home/omega/apps/conf/OmegaIps.conf

yes | cp -rf /home/omega/OmegaInstall/install/apps/conf/ipsLogback.properties /home/omega/apps/conf/

echo "Omega PS2/Ips Installed"