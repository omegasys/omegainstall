#!/bin/bash

Check_PS2 ()
{
  # print question
  echo -n "Is Omega PS2 needed? (y/n): "

  # read answer
  read YnAnswer

  # all to lower case
  YnAnswer=$(echo $YnAnswer | awk '{print tolower($0)}')

  # check and act on given answer
  case $YnAnswer in
    "y")  Yes_Check_Connect ;;
    "n")  No_Check_Connect ;;
    *)      echo "Please answer y or n" ; Check_PS2 ;;
  esac
}

Yes_Check_Connect ()
{
  # print question
  echo -n "Is Omega Connect needed? (y/n): "

  # read answer
  read YnAnswer

  # all to lower case
  YnAnswer=$(echo $YnAnswer | awk '{print tolower($0)}')

  # check and act on given answer
  case $YnAnswer in
    "y")
        sh ~/OmegaInstall/install/scripts/install-ps.sh
        sh ~/OmegaInstall/install/scripts/install-connect.sh
        sh ~/OmegaInstall/install/scripts/install-ips.sh
        break ;;
    "n")
        sh ~/OmegaInstall/install/scripts/install-ps.sh
        sh ~/OmegaInstall/install/scripts/install-ips.sh
        break ;;
    *)      echo "Please answer y or n" ; Yes_Check_Connect ;;
  esac
}

No_Check_Connect ()
{
  # print question
  echo -n "Is Omega Connect needed? (y/n): "

  # read answer
  read YnAnswer

  # all to lower case
  YnAnswer=$(echo $YnAnswer | awk '{print tolower($0)}')

  # check and act on given answer
  case $YnAnswer in
    "y")
        sh ~/OmegaInstall/install/scripts/install-ps.sh
        sh ~/OmegaInstall/install/scripts/install-connect.sh
        break;
        ;;
    "n")
        sh ~/OmegaInstall/install/scripts/install-ps.sh
        break ;;
    *)      echo "Please answer y or n" ; No_Check_Connect ;;
  esac
}


# entry
Check_PS2