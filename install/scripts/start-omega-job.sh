#!/bin/bash
#VARIABLES
jar_file="$1"
port="$2"
cwd=/home/omega/uploads
pid_file=/home/omega/apps/OmegaJob_pid.file
APP_NAME=OmegaJob
if [[ -z "$jar_file" ]]; then
  echo "Invalid jar file provided"
  exit 0
fi

if [[ -z "$port" ]]; then
  PORT_STARTUP=9090
else
  PORT_STARTUP="$port"
fi

echo "Current Directory is $cwd"
if [[ $jar_file == Omega* ]]; then
  echo "Here is filename only so append the current path and make it full path"
  #cwd=`pwd`
  filename="$cwd/$jar_file"
else
  filename=$jar_file
fi
echo "Fullpath is $filename"

PIDS=$(ps aux | grep java | grep "$APP_NAME" | awk {'print $2'})

echo "Going to stop previous application"
if [ -z "$PIDS" ]; then
  echo "The application is not running. Skip stopping"
else
  echo " Stopping pid: $PIDS"
  kill -9 "$PIDS"
  echo "OmegaJob stopped"
fi

#create version txt
mkdir -p /home/omega/apps/OmegaJob
jar xvf "$filename" BOOT-INF/classes/version.txt
cp -f BOOT-INF/classes/version.txt /home/omega/apps/OmegaJob
rm -rf BOOT-INF


echo "Now going to start application"
java -jar "$filename" -Dserver.port="$PORT_STARTUP" &
echo $! >"$pid_file" &

echo "Application running with pid $(cat $pid_file)"
