#!/bin/bash

# Function to check if a file contains specific text
check_file_for_text() {
    local file="$1"
    local text="$2"

    if grep -q "$text" "$file"; then
        echo "The file '$file' contains the text '$text'."
        return 0
    else
        echo "The file '$file' does not contain the text '$text'."
        return 1
    fi
}
