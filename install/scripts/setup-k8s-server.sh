#!/usr/bin/env bash


#sudo yum install git -y
sudo yum install wget -y
sudo yum install telnet -y

# handy fix command if not already set so
# sometimes might need to
sudo chown -R omega:omega /home/omega

sh ~/OmegaInstall/install/scripts/setup-server.sh

cd ~/OmegaInstall/

sh DockerV2/install-docker.sh
sh DockerV4/install-k8s.sh

#docker login

cd ..

docker pull omegasystem/omegabase:1.0