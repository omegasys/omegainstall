#!/bin/bash

Check_CMS ()
{
  # print question
  echo -n "Is Omega CMS needed? (y/n): "

  # read answer
  read YnAnswer

  # all to lower case
  YnAnswer=$(echo $YnAnswer | awk '{print tolower($0)}')

  # check and act on given answer
  case $YnAnswer in
    "y")  Yes_Check_Core4 ;;
    "n")  No_Check_Core4 ;;
    *)      echo "Please answer y or n" ; Check_CMS ;;
  esac
}

Yes_Check_Core4 ()
{
  # print question
  echo -n "Is Omega Core4 needed? (y/n): "

  # read answer
  read YnAnswer

  # all to lower case
  YnAnswer=$(echo $YnAnswer | awk '{print tolower($0)}')

  # check and act on given answer
  case $YnAnswer in
    "y")
        sh ~/OmegaInstall/install/scripts/install-cms.sh
        sh ~/OmegaInstall/install/scripts/install-ics.sh
        sh ~/OmegaInstall/install/scripts/install-core4.sh
        break ;;
    "n")
        sh ~/OmegaInstall/install/scripts/install-cms.sh
        break ;;
    *)      echo "Please answer y or n" ; Yes_Check_Core4 ;;
  esac
}

No_Check_Core4 ()
{
  # print question
  echo -n "Is Omega Core4 needed? (y/n): "

  # read answer
  read YnAnswer

  # all to lower case
  YnAnswer=$(echo $YnAnswer | awk '{print tolower($0)}')

  # check and act on given answer
  case $YnAnswer in
    "y")
        sh ~/OmegaInstall/install/scripts/install-ics.sh
        sh ~/OmegaInstall/install/scripts/install-core4.sh
        break;
        ;;
    "n")
        echo "Nothing will be installed";
        break ;;
    *)      echo "Please answer y or n" ; No_Check_Core4 ;;
  esac
}

# entry
Check_CMS