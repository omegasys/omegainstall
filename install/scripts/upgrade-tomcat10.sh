#!/bin/bash
set -e  # Detener la ejecución si ocurre un error

# Configuración
HOME="/home/omega"
INSTALL_DIR="$HOME/OmegaInstall/install"
TOMCAT_DIR="$HOME/tomcat"
CURRENT_TOMCAT_VERSION=$(ls -ld "$TOMCAT_DIR" | awk '{print $NF}')
NEW_TOMCAT_VERSION="10.1.30"
TOMCAT_URL="https://archive.apache.org/dist/tomcat/tomcat-10/v${NEW_TOMCAT_VERSION}/bin/apache-tomcat-${NEW_TOMCAT_VERSION}.tar.gz"

# Función para crear setenv.sh
create_setenv_script() {
    local setenv_path="$TOMCAT_DIR/bin/setenv.sh"
    local setenv_custom_path="$TOMCAT_DIR/bin/setenv-custom.sh"
    local setenv_source="$INSTALL_DIR/apps/bin/setenv-tomcat10.sh"

    if [ ! -f "$setenv_source" ]; then
        echo "Error: $setenv_source not found!"
        exit 1
    fi

    cp "$setenv_source" "$setenv_path"
    chmod +x "$setenv_path"
    echo "setenv.sh default Omega file created at $setenv_path"

    if [ ! -f "$setenv_custom_path" ]; then
        cat <<EOF > "$setenv_custom_path"
#!/bin/sh
# Custom environment variables for Tomcat (This file will not be overwritten in updates)

# Example: Customize memory settings
# export EXTRA_VAR="-XX:+UseStringDeduplication"
EOF
        chmod +x "$setenv_custom_path"
        echo "setenv-custom.sh file created at $setenv_custom_path (This file will remain unchanged)"
    fi
}

# Función para restaurar archivos de configuración
restore_config_files() {
    echo "Restoring configuration files..."

    if [ -f "$CURRENT_TOMCAT_VERSION/conf/server.xml" ]; then
        cp "$CURRENT_TOMCAT_VERSION/conf/server.xml" "$TOMCAT_DIR/conf/server.xml"
        echo "Restored server.xml"
    else
        echo "Warning: server.xml not found in $CURRENT_TOMCAT_VERSION"
    fi

    echo "Restoring log folder..."
    if [ ! -L "$TOMCAT_DIR/logs" ]; then
        cp -r "$CURRENT_TOMCAT_VERSION/logs" "$TOMCAT_DIR/"
    fi

    echo "Restoring tmp folder..."
    if [ ! -L "$TOMCAT_DIR/temp" ]; then
        cp -r "$CURRENT_TOMCAT_VERSION/temp" "$TOMCAT_DIR/"
    fi

    mkdir -p "$TOMCAT_DIR/conf/Catalina/localhost/" > /dev/null 2>&1

    cp -rf "$CURRENT_TOMCAT_VERSION/conf/Catalina/localhost/"*.xml "$TOMCAT_DIR/conf/Catalina/localhost/" 2>/dev/null
    cp -rf "$CURRENT_TOMCAT_VERSION/webapps/ROOT/"* "$TOMCAT_DIR/webapps/ROOT/" > /dev/null 2>&1
    cp -rf "$CURRENT_TOMCAT_VERSION/conf/context.xml" "$TOMCAT_DIR/conf/context.xml" > /dev/null 2>&1
    cp -rf "$CURRENT_TOMCAT_VERSION/conf/web.xml" "$TOMCAT_DIR/conf/web.xml" > /dev/null 2>&1
}

# Función para instalar o actualizar Tomcat
install_or_update_tomcat() {
    echo "Downloading Tomcat version $NEW_TOMCAT_VERSION..."
    if ! curl -o "/home/omega/apache-tomcat-${NEW_TOMCAT_VERSION}.tar.gz" -L "${TOMCAT_URL}"; then
        echo "Error: Failed to download Tomcat."
        exit 1
    fi

    if ! tar -zxf "/home/omega/apache-tomcat-${NEW_TOMCAT_VERSION}.tar.gz" -C "/home/omega/"; then
        echo "Error: Failed to extract Tomcat."
        exit 1
    fi
    rm "/home/omega/apache-tomcat-${NEW_TOMCAT_VERSION}.tar.gz"

    # Crear enlace simbólico
    ln -sfn "/home/omega/apache-tomcat-${NEW_TOMCAT_VERSION}" "$TOMCAT_DIR"

    # Crear el script setenv.sh
    create_setenv_script

    # Restaurar archivos de configuración
    restore_config_files

    # Cambiar el puerto predeterminado a 8081
    sed -i -e 's/8080/8081/g' "$TOMCAT_DIR/conf/server.xml"
}

# Iniciar el proceso de instalación o actualización
install_or_update_tomcat