#!/bin/bash

#
# main install script for all applications
#

echo "************************************************"
echo "Please choose which server you are deploying"
echo "************************************************"

PS3='Please pick an option: '
options=("Back Office/Core4" "PS/Connect/PS2" "Only Connect" "Tron/Tron2" "XAPI" "Quit")
select opt in "${options[@]}"
do
    case "$REPLY" in
        1)
            echo "*******************************************"
            echo "Deploying to $opt"
            echo "*******************************************"
            sh ~/OmegaInstall/install/scripts/backoffice-prompt.sh
            break;
            ;;
        2)
            echo "*******************************************"
            echo "Deploying to $opt"
            echo "*******************************************"
            sh ~/OmegaInstall/install/scripts/ps-prompt.sh
            break;
            ;;

        3)
            echo "*******************************************"
            echo "Deploying to $opt"
            echo "*******************************************"
            sh ~/OmegaInstall/install/scripts/install-connect.sh
            break;
            ;;

        4)
            echo "*******************************************"
            echo "Deploying to $opt"
            echo "*******************************************"
            sh ~/OmegaInstall/install/scripts/tron-prompt.sh
            break;
            ;;
        5)
            echo "*******************************************"
            echo "Deploying to $opt"
            echo "*******************************************"
            sh ~/OmegaInstall/install/scripts/install.xs.sh
            break;
            ;;
        6)
            break
            ;;
        *) echo "Invalid Option $REPLY";;
    esac
done


