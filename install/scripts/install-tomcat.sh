#dvariables

TOMCAT_VERSION=apache-tomcat-10.1.30

new_tomcat_input="$1"

if [[ "$new_tomcat_input" ]]; then
  TOMCAT_VERSION="$new_tomcat_input"
fi


HOME=/home/omega
INSTALL_DIR=$HOME/OmegaInstall/install
TOMCAT_TARGET_INSTALL_DIRECTORY=$HOME/tomcat
TOMCAT_DIRECTORY_OLD=$HOME/tomcat_old
TOMCAT_ZIP=$INSTALL_DIR/code/"$TOMCAT_VERSION.tar.gz"



if [ -d "$TOMCAT_TARGET_INSTALL_DIRECTORY" ]; then
  echo "Move existing tomcat directory to tomcat_old..."
  mv $TOMCAT_TARGET_INSTALL_DIRECTORY $TOMCAT_DIRECTORY_OLD
fi

if [ ! -f "$TOMCAT_ZIP" ]; then
  # Create new directory if not found
  echo "Tomcat zip $TOMCAT_ZIP does not exist..."
fi

# Create new directory if not found
echo "changing directory to $HOME"
cd $HOME || exit

echo "unzip $TOMCAT_ZIP"
tar -xvf $TOMCAT_ZIP

mv $HOME/$TOMCAT_VERSION $TOMCAT_TARGET_INSTALL_DIRECTORY
echo "Moving $HOME/$TOMCAT_VERSION to $TOMCAT_TARGET_INSTALL_DIRECTORY"

# remove examples folder
rm -r $TOMCAT_TARGET_INSTALL_DIRECTORY/webapps/examples

# copy the setenv.sh over to tomcat/bin folder
echo "copy $INSTALL_DIR/apps/bin/setenv.sh to $TOMCAT_TARGET_INSTALL_DIRECTORY/bin/setenv.sh"
cp $INSTALL_DIR/apps/bin/setenv.sh $TOMCAT_TARGET_INSTALL_DIRECTORY/bin/setenv.sh

#echo "copy $INSTALL_DIR/code/server.xml $TOMCAT_TARGET_INSTALL_DIRECTORY/conf/server.xml"
cp -rf $INSTALL_DIR/files/server.xml $TOMCAT_TARGET_INSTALL_DIRECTORY/conf/server.xml
cp -rf $INSTALL_DIR/files/context.xml $TOMCAT_TARGET_INSTALL_DIRECTORY/conf/context.xml
cp -rf $INSTALL_DIR/files/web.xml $TOMCAT_TARGET_INSTALL_DIRECTORY/conf/web.xml

if [ ! -d "/home/omega/tomcat/conf/Catalina" ]; then
  mkdir $TOMCAT_TARGET_INSTALL_DIRECTORY/conf/Catalina
fi
if [ ! -d "/home/omega/tomcat/conf/Catalina/localhost" ]; then
  mkdir $TOMCAT_TARGET_INSTALL_DIRECTORY/conf/Catalina/localhost
fi
if [ ! -d "/home/omega/tomcat/conf/webapps/ROOT/error-page" ]; then
   mkdir $TOMCAT_TARGET_INSTALL_DIRECTORY/webapps/ROOT/error-page
fi

cp -rf $INSTALL_DIR/files/ROOT/Error*.html $TOMCAT_TARGET_INSTALL_DIRECTORY/webapps/ROOT/error-page/
cp -rf $INSTALL_DIR/files/ROOT/general-error*.html $TOMCAT_TARGET_INSTALL_DIRECTORY/webapps/ROOT/error-page/
cp -rf $INSTALL_DIR/files/ROOT/index.jsp $TOMCAT_TARGET_INSTALL_DIRECTORY/webapps/ROOT/index.jsp



echo "Set max / min mem in setenv file"

mem_kb=$(grep MemTotal /proc/meminfo | awk '{print $2}')
mem_mb=$((mem_kb / 1024))
echo "Total memory: ${mem_mb} mb"
max_r=0.5
min_r=0.5
max_heap=$(echo $mem_mb $max_r | awk '{printf "%d\n", $1*$2}')
min_heap=$(echo $mem_mb $min_r | awk '{printf "%d\n", $1*$2}')

max_s=s/MAX_HEAP=.*/MAX_HEAP=${max_heap}/
sed -i -e "$max_s" $TOMCAT_TARGET_INSTALL_DIRECTORY/bin/setenv.sh
min_s=s/MIN_HEAP=.*/MIN_HEAP=${min_heap}/
sed -i -e "$min_s" $TOMCAT_TARGET_INSTALL_DIRECTORY/bin/setenv.sh

# change port to 8081
sed -i -e 's/8080/8081/g' /home/omega/tomcat/conf/server.xml
