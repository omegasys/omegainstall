#!/bin/bash

echo "*******************************************"
echo "Installing Omega PS"
echo "*******************************************"

# deploy code
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! -d "/home/omega/apps/OmegaPs" ]; then
mkdir /home/omega/apps/OmegaPs
fi

yes | cp -rf /home/omega/OmegaInstall/install/apps/ps.xml /home/omega/tomcat/conf/Catalina/localhost

if [ ! -d "/home/omega/apps/conf/" ]; then
mkdir /home/omega/apps/conf/
fi

# create ps conf
if [ -e "/home/omega/apps/conf/OmegaPs.conf" ]; then
rm /home/omega/apps/conf/OmegaPs.conf
fi
echo "jdbcUrl=jdbc:jtds:sqlserver://"$DBCONNECTION >> /home/omega/apps/conf/OmegaPs.conf
echo "user=admin_all
password=admin_all
" >> /home/omega/apps/conf/OmegaPs.conf

# cp troubleshooting script
yes | cp -rf /home/omega/OmegaInstall/install/apps/bin/showBusy.sh /home/omega/bin
chmod +x /home/omega/bin/showBusy.sh

echo "Omega PS Installed"