#!/bin/bash

# Get app name from command line
if test -z "$1"; then
  echo "Please specify the application (ps, txs or core)"
  exit
fi

APPNAME=$(echo "$1" | tr '[:upper:]' '[:lower:]')
export APPNAME

# 64 bit
CODE_DIR=$SCRIPTS_DIR/../code
JAVA_DIR=$SCRIPTS_DIR/../../java
SCRIPTS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
MAVEN_DIR=$SCRIPTS_DIR/../../maven
HOME_DIR=$(eval echo ~${SUDO_USER})

#Installing Java
echo "Installing Java"
"$SCRIPTS_DIR"/install-java.sh
echo "Java Installed"

#Installing Tomcat
echo "Installing Tomcat"
"$SCRIPTS_DIR"/install-tomcat.sh
echo "Tomcat Installed"

#create appropriate folders
cd ~
mkdir apps backups logs bin temp

#set DB connection in environment
yes | cp -rf /home/omega/OmegaInstall/install/apps/bin/updateDBConnection.sh /home/omega/bin
chmod +x /home/omega/bin/updateDBConnection.sh
sh /home/omega/bin/updateDBConnection.sh
source ~/.bash_profile

mkdir -p /home/omega/tomcat/conf/Catalina/localhost/

echo "Server Setup Complete!"
echo "Please close the session and login again BEFORE installing applications"
