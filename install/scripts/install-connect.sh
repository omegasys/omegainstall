#!/bin/bash

echo "*******************************************"
echo "Installing Omega Connect"
echo "*******************************************"

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# deploy code
if [ ! -d "/home/omega/apps/OmegaConnect/" ]; then
mkdir /home/omega/apps/OmegaConnect
fi

yes | cp -rf /home/omega/OmegaInstall/install/apps/connect.xml /home/omega/tomcat/conf/Catalina/localhost

if [ ! -d "/home/omega/apps/conf/" ]; then
mkdir /home/omega/apps/conf/
fi

if [ -e "/home/omega/apps/conf/OmegaConnect.conf" ]; then
rm /home/omega/apps/conf/OmegaConnect.conf
fi
echo "jdbcUrl=jdbc:jtds:sqlserver://"$DBCONNECTION >> /home/omega/apps/conf/OmegaConnect.conf
echo "user=admin_all
password=admin_all
" >> /home/omega/apps/conf/OmegaConnect.conf

echo "Omega Connect Installed"
