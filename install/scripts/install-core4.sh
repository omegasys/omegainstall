#!/bin/bash

echo "*******************************************"
echo "Installing Omega CORE4"
echo "*******************************************"

# deploy code
if [ ! -d "/home/omega/apps/OmegaCore" ]; then
mkdir /home/omega/apps/OmegaCore
fi

# deploy code
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

yes | cp -rf /home/omega/OmegaInstall/install/apps/core.xml /home/omega/tomcat/conf/Catalina/localhost
yes | cp -rf /home/omega/OmegaInstall/install/apps/agency.xml /home/omega/tomcat/conf/Catalina/localhost/

yes | cp -rf /home/omega/OmegaInstall/install/files/ROOT/index_core.jsp /home/omega/tomcat/webapps/ROOT/index.jsp

echo "Omega CORE4 Installed"