#!/bin/bash

# Variables
HOME=/home/omega
SERVICE_NAME="tomcat.service"
SERVICE_PATH="/etc/systemd/system/${SERVICE_NAME}"
TOMCAT_HOME="/home/omega/tomcat"
TOMCAT_DIR="$HOME/tomcat"
JAVA_HOME="/home/omega/java"
USER="omega"
GROUP="omega"

# Create the systemd service file
echo "Creating systemd service file for Tomcat..."

cat <<EOF | sudo tee $SERVICE_PATH
# Systemd unit file for Tomcat
[Unit]
Description=Apache Tomcat Web Application Container
After=syslog.target network.target

[Service]
Type=forking
Environment=JAVA_HOME=${JAVA_HOME}
Environment=CATALINA_PID=${TOMCAT_HOME}/temp/tomcat.pid
Environment=CATALINA_HOME=${TOMCAT_HOME}
Environment=CATALINA_BASE=${TOMCAT_HOME}

ExecStart=${TOMCAT_HOME}/bin/startup.sh
ExecStop=/bin/kill -15 \$MAINPID

User=${USER}
Group=${GROUP}
LimitNOFILE=65000
UMask=0007
RestartSec=10
Restart=always

[Install]
WantedBy=multi-user.target
EOF

# Set permissions
echo "Setting permissions for the service file..."
sudo chmod 644 $SERVICE_PATH
sudo find -L /home/omega/tomcat -type f -name "*.sh" | xargs -I {} chmod 755 {}

# Make sure SETENV.SH is correct

create_setenv_script() {
    local setenv_path="$TOMCAT_DIR/bin/setenv.sh"

    cat <<EOF > "$setenv_path"
#!/bin/sh
# Environment configuration for Tomcat

CATALINA_HOME="/home/omega/tomcat"
CATALINA_BASE="/home/omega/tomcat"
JAVA_HOME="/home/omega/java"

# Memory settings: allocate 75% of system memory to Tomcat
MAX_HEAP=$(($(grep MemTotal /proc/meminfo | awk '{print $2}') * 60 / 100 / 1024))
MIN_HEAP=$(($(grep MemTotal /proc/meminfo | awk '{print $2}') * 50 / 100 / 1024))

# Heap dump options
DMP_OPTS="-XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/omega/logs/heapdumps"

# Placeholder for any JMX-related options (optional, customize if needed)
JMX_OPTS=""

# Aggregate options for Tomcat
CATALINA_OPTS="\${DMP_OPTS} \${JMX_OPTS}"

# Get IP for JMX remote access (optional if JMX is needed)
IP=$(ip -o -4 addr show scope global | awk '{print $4}' | grep -v '^127.' | head -n 1 | cut -d '/' -f 1)

# JVM options
JVM_OPTS="-server -Xmx\${MAX_HEAP}m -Xms\${MIN_HEAP}m -XX:+UseG1GC -Djava.util.Arrays.useLegacyMergeSort=true -Xlog:gc*:file=/home/omega/logs/omegaGC.log:time,level,tags -Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom"

# JMX configuration (if required for monitoring)
ZABBIX_OPTS="-Djava.rmi.server.hostname=\${IP} -Dcom.sun.management.jmxremote.rmi.port=10052 -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=12345 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"

# Setting JAVA_OPTS to include all relevant options
JAVA_OPTS="\${JVM_OPTS} \${ZABBIX_OPTS}"

# Export necessary environment variables
export JAVA_HOME CATALINA_HOME CATALINA_BASE CATALINA_OPTS JAVA_OPTS
EOF

    chmod +x "$setenv_path"
    echo "setenv.sh file created at $setenv_path"
}

create_setenv_script

# Reload systemd to recognize the new service
echo "Reloading systemd..."
sudo systemctl daemon-reload

# Enable and start the Tomcat service
echo "Enabling and starting the Tomcat service..."
sudo systemctl enable $SERVICE_NAME
sudo systemctl restart $SERVICE_NAME

# Check service status
echo "Checking the status of the Tomcat service..."
sudo systemctl status $SERVICE_NAME