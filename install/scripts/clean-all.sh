#!/bin/bash
#
# script to clean up all installed applications to re-install
# You should only execute this script if you know what you are doing
#
echo "*******************************************************"
echo "You are about to clean all installed Omega applications"
echo "*******************************************************"

Check ()
{
  # print question
  echo -n "Are you sure? (yes/no): "

  # read answer
  read YnAnswer

  # all to lower case
  YnAnswer=$(echo $YnAnswer | awk '{print tolower($0)}')

  # check and act on given answer
  case $YnAnswer in
    "yes")
        rm -rf /home/omega/apps/*
        rm -rf /home/omega/bin/deploy* /home/omega/bin/display*
        rm -rf /home/omega/tomcat/conf/Catalina/localhost/*
        yes | cp -rf /home/omega/OmegaInstall/install/code/index.jsp /home/omega/tomcat/webapps/ROOT/index.jsp
        break;;
    "no")
        echo "No action performed."
        break ;;
    *)      echo "Please answer yes or no" ; Check ;;
  esac
}

# entry
Check