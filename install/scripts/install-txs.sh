#!/bin/bash

echo "*******************************************"
echo "Installing Omega Tron2/TXS"
echo "*******************************************"

# deploy code
SCRIPTS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

if [ ! -d "/home/omega/apps/OmegaTxs" ]; then
  mkdir -p /home/omega/apps/OmegaTxs
fi

yes | cp -rf /home/omega/OmegaInstall/install/apps/omegatxs.xml /home/omega/tomcat/conf/Catalina/localhost

if [ ! -d "/home/omega/apps/conf/" ]; then
  mkdir -p /home/omega/apps/conf/
fi

if [ -e "/home/omega/apps/conf/OmegaTxs.conf" ]; then
  rm /home/omega/apps/conf/OmegaTxs.conf
fi
echo "jdbcUrl=jdbc:jtds:sqlserver://"$DBCONNECTION >>/home/omega/apps/conf/OmegaTxs.conf
echo "user=admin_all
password=admin_all
" >>/home/omega/apps/conf/OmegaTxs.conf

yes | cp -rf /home/omega/OmegaInstall/install/apps/conf/txsLogback.properties /home/omega/apps/conf/

echo "Omega Tron2/TXS Installed"
