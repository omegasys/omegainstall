KAFKA_VERSION=3.1.0
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CODE_DIR=$SCRIPTS_DIR/../code
cd $CODE_DIR
echo "*******************************************"
echo "Downloading KAFKA $KAFKA_VERSION to  $CODE_DIR"
echo "*******************************************"

wget -c --header "Cookie: oraclelicense=accept-securebackup-cookie" https://dlcdn.apache.org/kafka/${KAFKA_VERSION}/kafka_2.13-3.1.0.tgz

