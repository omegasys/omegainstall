#!/bin/bash

Check_Tron() {
  # print question
  echo -n "Is Omega Tron needed? (y/n): "

  # read answer
  read YnAnswer

  # all to lower case
  YnAnswer=$(echo $YnAnswer | awk '{print tolower($0)}')

  # check and act on given answer
  case $YnAnswer in
  "y") Yes_Check_Tron2 ;;
  "n") No_Check_Tron2 ;;
  *)
    echo "Please answer y or n"
    Check_Tron
    ;;
  esac
}

Yes_Check_Tron2() {
  # print question
  echo -n "Is Omega Tron2 needed? (y/n): "

  # read answer
  read YnAnswer

  # all to lower case
  YnAnswer=$(echo $YnAnswer | awk '{print tolower($0)}')

  # check and act on given answer
  case $YnAnswer in
  "y")
    sh ~/OmegaInstall/install/scripts/install-omegatron.sh
    sh ~/OmegaInstall/install/scripts/install-txs.sh
    break
    ;;
  "n")
    sh ~/OmegaInstall/install/scripts/install-omegatron.sh
    break
    ;;
  *)
    echo "Please answer y or n"
    Yes_Check_Tron2
    ;;
  esac
}

No_Check_Tron2() {
  # print question
  echo -n "Is Omega Tron2 needed? (y/n): "

  # read answer
  read YnAnswer

  # all to lower case
  YnAnswer=$(echo $YnAnswer | awk '{print tolower($0)}')

  # check and act on given answer
  case $YnAnswer in
  "y")
    sh ~/OmegaInstall/install/scripts/install-txs.sh
    break
    ;;
  "n")
    echo "Nothing will be installed"
    break
    ;;
  *)
    echo "Please answer y or n"
    No_Check_Tron2
    ;;
  esac
}

# entry
Check_Tron
