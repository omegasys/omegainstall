ZK_VERSION=3.8.0
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CODE_DIR=$SCRIPTS_DIR/../code
cd $CODE_DIR
echo "*******************************************"
echo "Downloading KAFKA $ZK_VERSION to  $CODE_DIR"
echo "*******************************************"

wget -c --header "Cookie: oraclelicense=accept-securebackup-cookie" https://dlcdn.apache.org/zookeeper/zookeeper-$ZK_VERSION/apache-zookeeper-$ZK_VERSION-bin.tar.gz

