#!/bin/bash

echo "*******************************************"
echo "Installing Omega ICS"
echo "*******************************************"

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# deploy code
if [ ! -d "/home/omega/apps/OmegaIcs" ]; then
mkdir /home/omega/apps/OmegaIcs
fi

yes | cp -rf /home/omega/OmegaInstall/install/apps/ics.xml /home/omega/tomcat/conf/Catalina/localhost/

if [ ! -d "/home/omega/apps/conf/" ]; then
mkdir /home/omega/apps/conf/
fi

if [ -e "/home/omega/apps/conf/OmegaIcs.conf" ]; then
rm /home/omega/apps/conf/OmegaIcs.conf
fi
echo "jdbcUrl=jdbc:jtds:sqlserver://"$DBCONNECTION";useLOBs=false" >> /home/omega/apps/conf/OmegaIcs.conf
echo "user=admin_all
password=admin_all
" >> /home/omega/apps/conf/OmegaIcs.conf

# cp logback file
yes | cp -rf /home/omega/OmegaInstall/install/apps/conf/ics_logback.properties /home/omega/apps/conf/

echo "Omega ICS Installed"