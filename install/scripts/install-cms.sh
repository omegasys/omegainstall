#!/bin/bash

echo "*******************************************"
echo "Installing Omega CMS"
echo "*******************************************"

# deploy code
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! -d "/home/omega/apps/OmegaCms" ]; then
mkdir /home/omega/apps/OmegaCms
fi

yes | cp -rf /home/omega/OmegaInstall/install/apps/j.xml /home/omega/tomcat/conf/Catalina/localhost/

if [ ! -d "/home/omega/apps/conf" ]; then
mkdir /home/omega/apps/conf
fi

if [ -e "/home/omega/apps/conf/OmegaCms.conf" ]; then
rm /home/omega/apps/conf/OmegaCms.conf
fi
echo "jdbcUrl=jdbc:jtds:sqlserver://"$DBCONNECTION";useLOBs=false" >> /home/omega/apps/conf/OmegaCms.conf
echo "user=admin_all
password=admin_all
" >> /home/omega/apps/conf/OmegaCms.conf

yes | cp -rf /home/omega/OmegaInstall/install/apps/conf/quartz.properties /home/omega/apps/conf/
echo "org.quartz.dataSource.omegaDs.URL = jdbc:jtds:sqlserver://"$DBCONNECTION >> /home/omega/apps/conf/quartz.properties
echo "org.quartz.dataSource.omegaDs.user = admin_all
org.quartz.dataSource.omegaDs.password = admin_all
" >> /home/omega/apps/conf/quartz.properties

yes | cp -rf /home/omega/OmegaInstall/install/code/index_cms.jsp /home/omega/tomcat/webapps/ROOT/index.jsp

echo "Omega CMS Installed"
