JDK=jdk-8u191-linux-x64.tar.gz
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CODE_DIR=$SCRIPTS_DIR/../code
cd $CODE_DIR
echo "*******************************************"
echo "Downloading  $JDK to  $CODE_DIR"
echo "*******************************************"

wget -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u191-b12/2787e4a523244c269598db4e85c51e0c/jdk-8u191-linux-x64.tar.gz

