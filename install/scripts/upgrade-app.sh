#!/bin/bash

main() {
  VERSION="$1"
  TYPE_DEPLOY="$2"
  APP="$3"
  HOME_FOLDER="/home/omega"
  UPLOADS_FOLDER="$HOME_FOLDER/uploads"

  # INITIAL CODE
  if [[ "$TYPE_DEPLOY" == "single" ]]; then
    deploy_single_app "$APP" "$VERSION"

  else
    deploy_ps_server "$VERSION"
    deploy_txs_server "$VERSION"
    deploy_core_server "$VERSION"
  fi

}

upgrade_app() {
  application=$1
  version=$2
  APP_AND_VERSION="$application-$version.war"
  dest="$UPLOADS_FOLDER/$APP_AND_VERSION"
  echo "Backup application $dest"
  mv "$dest" "$dest"_"$(date +%Y%m%d%H%M)"
  echo "Downloading $application with version $version..."
  java -jar "/home/omega/OmegaInstall/misc/s3-copy.jar builds.omegasys.eu/omega-apps/$2/$APP_AND_VERSION" "$dest"
}

upgrade_core() {
  application=$1
  version=$2
  dest="$UPLOADS_FOLDER/$application"
  echo "Backup application $dest"
  mv "$dest" "$dest"_"$(date +%Y%m%d%H%M)"
  echo "Downloading $application with version $version..."
  java -jar "/home/omega/OmegaInstall/misc/s3-copy.jar builds.omegasys.eu/omega-apps/$2/$application" "$dest"
}

app_match() {
  app="$1"
  if [[ ${AVAILABLE_APPS[*]} =~ $app ]]; then
    return 0
  else
    return 1
  fi
}

deploy_ps_server() {
  arr=()
  version="$1"
  ##DEPLOY PS SERVER
  if [[ -f "/home/omega/tomcat/conf/Catalina/localhost/ps.xml" ]]; then
    upgrade_app "OmegaPs" "$version"
    arr+=("OmegaPs-$version.war")
  fi
  if [[ -f "/home/omega/tomcat/conf/Catalina/localhost/ips.xml" ]]; then
    upgrade_app "OmegaIps" "$version"
    arr+=("OmegaIps-$version.war")
  fi
  if [[ -f "/home/omega/tomcat/conf/Catalina/localhost/connect.xml" ]]; then
    upgrade_app "OmegaConnect" "$version"
    arr+=("OmegaConnect-$version.war")

  fi

  deploy.sh "${arr[@]}"
}

deploy_txs_server() {
  version="$1"
  arr=()
  ##DEPLOY TXS SERVER
  if [[ -f "/home/omega/tomcat/conf/Catalina/localhost/omegatxs.xml" ]]; then
    upgrade_app "OmegaTxs" "$version"
    arr+=("OmegaTxs-$version.war")

  fi
  if [[ -f "/home/omega/tomcat/conf/Catalina/localhost/omegatron.xml" ]]; then
    upgrade_app "OmegaTron" "$version"
    arr+=("OmegaTron-$version.war")
  fi

  deploy.sh "${arr[@]}"
  deployCore.sh
}

deploy_core_server() {
  version="$1"
  arr=()
  ##DEPLOY CORE SERVER
  if [[ -f "/home/omega/tomcat/conf/Catalina/localhost/j.xml" ]]; then
    upgrade_app "OmegaCms" "$version"
    arr+=("OmegaCms-$version.war")

  fi
  if [[ -f "/home/omega/tomcat/conf/Catalina/localhost/ics.xml" ]]; then
    upgrade_app "OmegaIcs" "$version"
    arr+=("OmegaIcs-$version.war")
  fi
  if [[ -f "/home/omega/tomcat/conf/Catalina/localhost/core.xml" ]]; then
    upgrade_core "core.tar.gz" "$version"
  fi

  if [[ -f "/home/omega/tomcat/conf/Catalina/localhost/ams.xml" ]]; then
    upgrade_app "OmegaAMS" "$version"
  fi

  deploy.sh "${arr[@]}"

}

deploy_single_app() {
  AVAILABLE_APPS=("OmegaPs" "OmegaIps" "OmegaConnect" "OmegaCms" "OmegaIcs" "core.tar.gz" "OmegaTron" "OmegaTxs")
  app="$1"
  version="$2"

  if app_match "$app"; then
    if [[ "$app" == "core.tar.gz" ]]; then
      upgrade_core "core.tar.gz" "$version"
    else
      upgrade_app "$app" "$version"
    fi
  else
    echo "Application provided is not valid. Please provide one of the following: " "${AVAILABLE_APPS[@]}"
    exit 1
  fi

}

main "$@"
exit
