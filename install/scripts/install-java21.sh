#!/bin/bash

# Update these variables for Java 21
INSTALL_DIR="/usr/lib/jvm/java-21-amazon-corretto"
JAVA_PACKAGE="java-21-amazon-corretto-devel"
PROFILE_SCRIPT="/etc/profile.d/java.sh"

check_and_update_java() {
    if [ -e "/home/omega/java" ]; then
        if [ -L "/home/omega/java" ]; then
            echo "/home/omega/java is a symlink."
        elif [ -d "/home/omega/java" ]; then
            echo "/home/omega/java is a directory. Adapting to proper structure..."
            mv /home/omega/java /home/omega/java_old
            ln -s /home/omega/java_old /home/omega/java
        else
            echo "/home/omega/java exists, but it is not symlink neither dir. Continue..."
        fi
    else
        echo "/home/omega/java does not exist yet. Continue..."
    fi
}

add_corretto_repo() {
    sudo rpm --import https://yum.corretto.aws/corretto.key
    sudo curl -o /etc/yum.repos.d/corretto.repo https://yum.corretto.aws/corretto.repo
}

install_java() {
    echo "Installing $JAVA_PACKAGE..."
    sudo yum install -y "$JAVA_PACKAGE" 
}

configure_environment() {
    echo "Configuring JAVA_HOME and PATH..."
    echo "export JAVA_HOME=$INSTALL_DIR" | sudo tee "$PROFILE_SCRIPT"
    echo "export PATH=\$JAVA_HOME/bin:\$PATH" | sudo tee -a "$PROFILE_SCRIPT"
    source "$PROFILE_SCRIPT"
}

verify_installation() {
    echo "Verifying Java installation..."
    java -version
}

symlink_create() {
    echo "Creating symlink..."
    cd /home/omega
    rm /home/omega/java
    ln -sf "$INSTALL_DIR" java
}

main() {
    check_and_update_java
    add_corretto_repo
    install_java
    configure_environment
    verify_installation
    symlink_create
    echo "Java 21 installed successfully."
}

main