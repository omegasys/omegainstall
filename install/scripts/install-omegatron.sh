#!/bin/bash

echo "*******************************************"
echo "Installing Omega Tron"
echo "*******************************************"

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# deploy code
if [ ! -d "/home/omega/apps/OmegaTron" ]; then
mkdir /home/omega/apps/OmegaTron
fi

yes | cp -rf /home/omega/OmegaInstall/install/apps/omegatron.xml /home/omega/tomcat/conf/Catalina/localhost

if [ ! -d "/home/omega/apps/conf/" ]; then
mkdir /home/omega/apps/conf/
fi

# create tron conf
if [ -e "/home/omega/apps/conf/OmegaTron.conf" ]; then
rm /home/omega/apps/conf/OmegaTron.conf
fi
echo "jdbcUrl=jdbc:jtds:sqlserver://"$DBCONNECTION >> /home/omega/apps/conf/OmegaTron.conf
echo "user=admin_all
password=admin_all
" >> /home/omega/apps/conf/OmegaTron.conf

# cp logback file
yes | cp -rf /home/omega/OmegaInstall/install/apps/conf/logback.properties /home/omega/apps/conf/

echo "Omega Tron Installed"