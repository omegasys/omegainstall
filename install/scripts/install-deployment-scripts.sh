#!/bin/bash
# Capture start time in seconds
start=$(date +%s)

echo "Start installing deployment scripts"
# cp troubleshooting script
echo "Installing... showBusy.sh"
cp -ri /home/omega/OmegaInstall/install/apps/bin/showBusy.sh /home/omega/bin
chmod +x /home/omega/bin/showBusy.sh

# cp deploy script
echo "Installing... schedule-deploy.sh"
cp -ri /home/omega/OmegaInstall/deploy/schedule-deploy.sh /home/omega/bin/
chmod +x /home/omega/bin/schedule-deploy.sh

echo "Installing... deploy.sh"
cp -ri /home/omega/OmegaInstall/install/apps/bin/deploy.sh /home/omega/bin/
chmod +x /home/omega/bin/deploy.sh

echo "Installing... deployJar.sh"
cp -ri /home/omega/OmegaInstall/install/apps/bin/deployJar.sh /home/omega/bin/
chmod +x /home/omega/bin/deployJar.sh

echo -n "Installing... deployCore.sh"
cp -ri /home/omega/OmegaInstall/install/apps/bin/deployCore.sh /home/omega/bin/
chmod +x /home/omega/bin/deployCore.sh
echo "OK"

echo -n "Installing... deployWar.sh"
cp -ri /home/omega/OmegaInstall/install/apps/bin/deployWar.sh /home/omega/bin/
chmod +x /home/omega/bin/deployWar.sh
echo "OK"

echo -n "Installing... deployJar.sh"
cp -ri /home/omega/OmegaInstall/install/apps/bin/deployJar.sh /home/omega/bin/
chmod +x /home/omega/bin/deployJar.sh
echo "OK"

echo -n "Installing... oget.sh"
cp -ri /home/omega/OmegaInstall/install/apps/bin/oget.sh /home/omega/bin
chmod +x /home/omega/bin/oget.sh
echo "OK"

echo -n "Installing... displayVersion.sh"
cp -ri /home/omega/OmegaInstall/install/apps/bin/displayVersion.sh /home/omega/bin
chmod +x /home/omega/bin/displayVersion.sh
echo "OK"

echo -n "Installing... startJar.sh"
cp -ri /home/omega/OmegaInstall/install/apps/bin/startJar.sh /home/omega/bin
chmod +x /home/omega/bin/startJar.sh
echo "OK"

echo -n "Installing... get_ip.sh"
cp -ri /home/omega/OmegaInstall/install/apps/bin/get_ip.sh /home/omega/bin
chmod +x /home/omega/bin/get_ip.sh
echo "OK"

echo -n "Installing... download.sh"
cp -ri /home/omega/OmegaInstall/install/apps/bin/download.sh /home/omega/bin
chmod +x /home/omega/bin/download.sh
echo "OK"

echo -n "Installing... s3-copy.jar"
cp -ri /home/omega/OmegaInstall/misc/s3-copy.jar /home/omega/bin
chmod +x /home/omega/bin/s3-copy.jar
echo "OK"

# Capture end time in seconds
end=$(date +%s)

# Calculate the time difference
runtime=$((end - start))

echo "Install deployment scripts Complete! :: Time taken: $runtime seconds"
