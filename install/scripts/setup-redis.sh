#!/bin/bash
# Ensure the script is run as root
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root. Use sudo or log in as root."
    exit 1
fi
# Function to handle errors
handle_error() {
    echo "An error occurred. Exiting..."
    exit 1
}
# Trap errors and call handle_error function
trap 'handle_error' ERR
# Define variables
USER_NAME="omega"
HOME_DIR="/home/$USER_NAME"
# Function to ask for Redis IP
ask_redis_ip() {
    read -p "Enter the Redis IP address (default: 127.0.0.1): " REDIS_IP
    REDIS_IP=${REDIS_IP:-127.0.0.1}
}
# Function to ask if replica is needed
ask_replica() {
    read -p "Do you want to configure a replica? (yes/no): " REPLICA_NEEDED
    if [[ "$REPLICA_NEEDED" =~ ^(yes|y)$ ]]; then
        read -p "Enter the replica IP address: " REPLICA_IP
    fi
}
# Function to ask for ports
ask_ports() {
    read -p "Enter the Redis port (default: 6379): " REDIS_PORT
    REDIS_PORT=${REDIS_PORT:-6379}
    read -p "Enter the Sentinel port (default: 26379): " SENTINEL_PORT
    SENTINEL_PORT=${SENTINEL_PORT:-26379}
}
# Function to preinstall required packages
preinstall() {
    yum update -y || handle_error
    yum install -y openssl-devel tar vim || handle_error
    systemctl stop firewalld || handle_error
    systemctl disable firewalld || handle_error
    # Configure sysctl settings for Redis
    echo "Configuring sysctl settings for Redis..."
    echo "vm.swappiness=60" | sudo tee /etc/sysctl.d/redis.conf || handle_error
    echo "vm.overcommit_memory=1" | sudo tee -a /etc/sysctl.d/redis.conf || handle_error
    # Apply sysctl changes
    sysctl -p /etc/sysctl.d/redis.conf || handle_error
}
# Function to set up Redis environment
redis_env() {
    mkdir -p /opt/redis/installation || handle_error
    cd /opt/redis/installation || handle_error
    curl -o redis-stack-server.tar.gz https://packages.redis.io/redis-stack/redis-stack-server-7.2.0-v9.rhel9.x86_64.tar.gz || handle_error
    tar -zxf redis-stack-server.tar.gz || handle_error
    cp -r ./redis-stack-server-7.2.0-v9/* /opt/redis || handle_error
    touch /opt/redis/etc/redis-stack-users.acl || handle_error
}
# Function to configure Redis
configure_redis() {
    cat <<EOF | sudo tee /opt/redis/etc/redis-stack.conf || handle_error
port $REDIS_PORT
bind 127.0.0.1 $REDIS_IP
tcp-keepalive 300
timeout 0
tcp-backlog 1024
maxclients 10000
pidfile /opt/redis/var/redis_$REDIS_PORT.pid
logfile "/opt/redis/var/db/redis-stack/redis-stack.log"
aclfile /opt/redis/etc/redis-stack-users.acl
loglevel notice
daemonize no
maxmemory 1500mb
protected-mode yes
maxmemory-policy noeviction
dir "/opt/redis/var"
dbfilename "redis-data.rdb"
rdbcompression yes
rdbchecksum yes
save 14400 0
stop-writes-on-bgsave-error yes
appendonly yes
appenddirname "redis-stack-aof"
appendfilename "redis-stack.aof"
#always, everysec, no
appendfsync always
auto-aof-rewrite-percentage 100
auto-aof-rewrite-min-size 64mb
aof-load-truncated no
aof-use-rdb-preamble yes
aof-timestamp-enabled yes
#Misc Settings
hash-max-listpack-entries 512
hash-max-listpack-value 64
list-max-listpack-size -2
repl-diskless-load disabled
repl-backlog-size 10mb
repl-backlog-ttl 3600
replica-priority 100
replica-announced yes
min-replicas-to-write 0
min-replicas-max-lag 10
lua-time-limit 5000
busy-reply-threshold 5000
EOF
    echo "export PATH=$PATH:/opt/redis/bin" >> /etc/profile || handle_error
    source /etc/profile || handle_error
    echo "Global Path updated:"
    echo $PATH
    ln -sf /opt/redis/bin/redis-cli /usr/local/sbin/redis-cli
    ln -sf /opt/redis/bin/redis-cli /usr/local/bin/redis-cli
    ln -sf /opt/redis/bin/redis-server /usr/local/sbin/redis-server
    ln -sf /opt/redis/bin/redis-server /usr/local/bin/redis-server
}
# Function to create Redis service
create_redis_service() {
    echo "Creating systemd service file for Redis..."
    cat <<EOF | sudo tee /opt/redis/redis-stack.service || handle_error
[Unit]
Description=Redis Stack 7
After=network.target
[Service]
Type=simple
ExecStart=/opt/redis/bin/redis-stack-server /opt/redis/etc/redis-stack.conf
ExecStop=/opt/redis/bin/redis-cli shutdown
Restart=always
[Install]
WantedBy=multi-user.target
EOF
    ln -sf /opt/redis/redis-stack.service /etc/systemd/system/redis-stack.service || handle_error
    systemctl daemon-reload || handle_error
    systemctl enable redis-stack || handle_error
    systemctl start redis-stack || handle_error
    systemctl status redis-stack || handle_error
}
# Function to configure default Redis user
configure_default_redis() {
    redis-cli <<EOF || handle_error
acl setuser default on >Default123$%^ allcommands allkeys
acl save
EOF
}
# Function to configure Redis replica
replica_redis_conf() {
    if [[ "$REPLICA_NEEDED" =~ ^(yes|y)$ ]]; then
        cat <<EOF | sudo tee /opt/redis/etc/redis-stack.conf || handle_error
# Configure Replication
masteruser default
masterauth Default123$%^
replicaof $REPLICA_IP $REDIS_PORT
EOF
        systemctl restart redis-stack || handle_error
        systemctl status redis-stack || handle_error
        echo "Checking if changes applied"
        redis-cli info replication || handle_error
    fi
}
# Function to configure firewall
configure_firewall() {
    systemctl start firewalld || handle_error
    firewall-cmd --add-port=$REDIS_PORT/tcp --permanent || handle_error
    firewall-cmd --add-port=$SENTINEL_PORT/tcp --permanent || handle_error
    firewall-cmd --reload || handle_error
}
# Function to configure Sentinel
configure_sentinel() {
    cat <<EOF | sudo tee /opt/redis/etc/redis-sentinel.conf || handle_error
port $SENTINEL_PORT
sentinel monitor masterInstance $REDIS_IP $REDIS_PORT 2
sentinel down-after-milliseconds masterInstance 5000
sentinel failover-timeout masterInstance 60000
#sentinel auth-user masterInstance default
sentinel auth-pass masterInstance Default123$%^
logfile "/opt/redis/var/db/redis-stack/redis-sentinel.log"
dir "/opt/redis/var"
loglevel notice
protected-mode no
daemonize no
pidfile "/opt/redis/var/redis-sentinel_$SENTINEL_PORT.pid"
# sentinel notification-script mymaster /var/redis/notify.sh
# sentinel announce-ip <ip>
# sentinel announce-port <port>
EOF
}
# Function to create Sentinel service
create_sentinel_service() {
    echo "Creating systemd service file for Redis Sentinel..."
    cat <<EOF | sudo tee /opt/redis/redis-sentinel.service || handle_error
[Unit]
Description=Redis Sentinel
After=network.target
[Service]
Type=simple
ExecStart=/opt/redis/bin/redis-sentinel /opt/redis/etc/redis-sentinel.conf
ExecStop=/opt/redis/bin/redis-cli -p $SENTINEL_PORT shutdown
Restart=always
[Install]
WantedBy=multi-user.target
EOF
    ln -sf /opt/redis/redis-sentinel.service /etc/systemd/system/redis-sentinel.service || handle_error
    systemctl daemon-reload || handle_error
    systemctl enable redis-sentinel || handle_error
    systemctl start redis-sentinel || handle_error
    systemctl status redis-sentinel || handle_error
}
function verify_install() {
    echo ""
    echo "Verifying redis client/server reply"
    sleep 2
    redis-cli ping
    echo ""
    sleep 2
    echo "Verifying sentinel conf..."
    sleep 2
    redis-cli -p $SENTINEL_PORT info Sentinel || handle_error
}
# Main script execution
ask_redis_ip
ask_replica
ask_ports
preinstall
redis_env
configure_redis
create_redis_service
configure_default_redis
replica_redis_conf
configure_firewall
configure_sentinel
create_sentinel_service
verify_install
echo ""
echo "Redis and Sentinel configuration completed successfully."