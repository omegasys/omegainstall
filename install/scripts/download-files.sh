#!/bin/bash

APP="$1"
VERSION="$2"
SINGLE_APP="$3"
JAR_FILE=/home/omega/OmegaInstall/misc/s3-download.jar
COPY_FILE=/home/omega/OmegaInstall/misc/s3-copy.jar
BUILDS_OMEGA_APPS_FOLDER=builds.omegasys.eu/omega-apps
BUILDS_CHRONOS_FOLDER=builds.omegasys.eu/chronos
DESTINATION_FOLDER=/home/omega/uploads
FULL_JAR=""
FULL_OUTPUT_JAR=""

# DOWNLOAD SINGLE APP APPLICATION
if [[ "$SINGLE_APP" ]]; then
  if [ "${APP}" == "core.tar.gz" ]; then
    FULL_OUTPUT_JAR="${DESTINATION_FOLDER}/${APP}"
    FULL_JAR="${BUILDS_OMEGA_APPS_FOLDER}/${VERSION}/${APP}"
  elif [[ "$APP" == *"chronos"* || "$APP" == *"neccton"* || "$APP" == *"lslb"* || "$APP" == *"cdb"* ]]; then
    FULL_JAR="${BUILDS_CHRONOS_FOLDER}/${VERSION}/${APP}-${VERSION}.jar"
    FULL_OUTPUT_JAR="${DESTINATION_FOLDER}/${APP}-${VERSION}.jar"
  else
    FULL_JAR="${BUILDS_OMEGA_APPS_FOLDER}/${VERSION}/${APP}-${VERSION}.war"
    FULL_OUTPUT_JAR="${DESTINATION_FOLDER}/${APP}-${VERSION}.war"
  fi

  java -jar "$COPY_FILE" "$FULL_JAR" "$FULL_OUTPUT_JAR"

else
  # DOWNLOAD BUNDLE APPLICATIONS
  java -jar "$JAR_FILE" "$VERSION" "$APP"
fi
