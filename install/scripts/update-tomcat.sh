#!/bin/bash

HOME=/home/omega
INSTALL_DIR=$HOME/OmegaInstall/install

# Source the functions script
source "${INSTALL_DIR}"/scripts/check-text-file.sh

# For example <TOMCAT VERSION> <COPY_OLD>
# ~/OmegaInstall/install/scripts/update-tomcat.sh 10.1.30 false

#DEFINITION
NEW_TOMCAT_VERSION=10.1.30
copy_old=true
#OBTAIN VERSION OF OLD TOMCAT
VERSION=$(cat /home/omega/tomcat/RELEASE-NOTES | grep "Apache Tomcat Version" | awk -F " " '{print substr($4,0)}')

#VARIABLES
new_tomcat_input="$1"
copy_old_input="$2"

if [[ "$new_tomcat_input" ]]; then
	NEW_TOMCAT_VERSION="$new_tomcat_input"
fi

if [[ "$copy_old_input" ]]; then
	copy_old="$copy_old_input"
fi
OLD_TOMCAT_DIR=/home/omega/apache-tomcat-"${VERSION}"
NEW_TOMCAT_DIR=/home/omega/apache-tomcat-"${NEW_TOMCAT_VERSION}"

# MOVE TOMCAT FOLDER TO TOMCAT-VERSION FOLDER AND CREATE SYMBOLIC LINK
if [ -d "/home/omega/tomcat" ]; then
	mv /home/omega/tomcat /home/omega/apache-tomcat-"${VERSION}"
	ln -s /home/omega/apache-tomcat-"${VERSION}" /home/omega/tomcat
elif [ -L "/home/omega/tomcat" ]; then
	rm "/home/omega/tomcat"
fi

# COPY TOMCAT VERSION AND UNZIP
cp /home/omega/OmegaInstall/install/code/apache-tomcat-"${NEW_TOMCAT_VERSION}".tar.gz /home/omega/
tar -xvf "${NEW_TOMCAT_DIR}.tar.gz"
rm "${NEW_TOMCAT_DIR}.tar.gz"
rm -r "${NEW_TOMCAT_DIR}/webapps/examples"

if $copy_old; then
	# IF WE WANT TO COPY CONFIGURATIONS FROM OLD TOMCAT
	cp "${OLD_TOMCAT_DIR}"/bin/setenv.sh "${NEW_TOMCAT_DIR}"/bin/setenv.sh
#	Change parameter that Java 21 don't support
	sed -i 's/-XX:+CMSClassUnloadingEnabled//g' "${NEW_TOMCAT_DIR}"/bin/setenv.sh
  sed -i 's/-XX:+PrintGCDateStamps//g' "${NEW_TOMCAT_DIR}"/bin/setenv.sh
  sed -i 's/-XX:+PrintGCDetails//g' "${NEW_TOMCAT_DIR}"/bin/setenv.sh
  sed -i 's/-Xloggc:omegaGC.log/-Xlog:gc:omegaGC.log/g' "${NEW_TOMCAT_DIR}"/bin/setenv.sh

	cp -rf "${OLD_TOMCAT_DIR}"/conf/server.xml "${NEW_TOMCAT_DIR}"/conf/server.xml
	cp -rf "${OLD_TOMCAT_DIR}"/conf/context.xml "${NEW_TOMCAT_DIR}"/conf/context.xml
else
	# INSTALL NEW CONFIGURATIONS
	sh install-tomcat.sh "${NEW_TOMCAT_DIR}"
	exit 0
fi

# CHECK IF CATALINA FOLDER EXISTS AND CREATE
if [ ! -d "${NEW_TOMCAT_DIR}/conf/Catalina" ]; then
	mkdir "${NEW_TOMCAT_DIR}/conf/Catalina"
fi
if [ ! -d "${NEW_TOMCAT_DIR}/conf/Catalina/localhost" ]; then
	mkdir "${NEW_TOMCAT_DIR}/conf/Catalina/localhost"
fi

# COPY OLD XML FILE TO NEW TOMCAT
cp "${OLD_TOMCAT_DIR}"/conf/Catalina/localhost/*.xml "${NEW_TOMCAT_DIR}/conf/Catalina/localhost/"

# COPY OLD ROOT Index jsp if exists
if [ -e "${OLD_TOMCAT_DIR}/webapps/ROOT/index.jsp" ] && ( check_file_for_text "${OLD_TOMCAT_DIR}/webapps/ROOT/index.jsp" "Hello! Server is running" || check_file_for_text "${OLD_TOMCAT_DIR}/webapps/ROOT/index.jsp" "response.sendRedirect" ); then
	cp "${OLD_TOMCAT_DIR}/webapps/ROOT/index.jsp" "${NEW_TOMCAT_DIR}/webapps/ROOT/index.jsp"
else
	cp -rf "$INSTALL_DIR/files/ROOT/index.jsp" "${NEW_TOMCAT_DIR}/webapps/ROOT/index.jsp"
fi

# COPY OLD ROOT Index jsp if exists
if [ -d "${OLD_TOMCAT_DIR}/webapps/ROOT/error-page" ]; then
	cp -r "${OLD_TOMCAT_DIR}/webapps/ROOT/error-page" "${NEW_TOMCAT_DIR}/webapps/ROOT/error-page"
else
	if [ ! -d "${NEW_TOMCAT_DIR}/webapps/ROOT/error-page" ]; then
		mkdir "${NEW_TOMCAT_DIR}/webapps/ROOT/error-page"
	fi
	cp -rf $INSTALL_DIR/files/ROOT/Error*.html "${NEW_TOMCAT_DIR}/webapps/ROOT/error-page"
	cp -rf $INSTALL_DIR/files/ROOT/general-error*.html "${NEW_TOMCAT_DIR}/webapps/ROOT/error-page"
fi

ln -sfn "${NEW_TOMCAT_DIR}" /home/omega/tomcat
