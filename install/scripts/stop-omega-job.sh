#!/bin/bash
APP_NAME=OmegaJob

PIDS=$(ps aux | grep java | grep "$APP_NAME" | awk {'print $2'})

echo "Going to stop previous application"
if [ -z "$PIDS" ]; then
  echo "The application is not running. Skip stopping"
else
  kill -9 "$PIDS"
fi

echo "OmegaJob  stopped"
