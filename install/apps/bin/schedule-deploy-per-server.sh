#!/usr/bin/env bash
#set -e
TODAYTIME="$(date +%F)-$(date +%T)"
RED='\033[0;31m'
GREEN='\033[0;32m'
APPLICATION_HOME="/home/omega/apps/"
UPLOAD="/home/omega/uploads"
CATALINA=/home/omega/tomcat/bin/catalina.sh
echo $PWD
echo $USER
cp install/apps/bin/deploy.sh ~/bin/
chmod +x  ~/bin/deploy.sh
if test -z "$1"
then
        echo "You must specify release version to deploy"
        exit
fi
# get release version from first input argument
BRANCH=$1
if test -z "$2"
then
        echo "You must specify applications to deploy"
        exit
fi
APPS=$2
echo "Applications to be deployed are:"
echo $APPS
echo "pass a space for an empty 3rd parameter to start tomcat"
echo $3
echo "Stopping tomcat"
pkill -9 -f tomcat
for NAME in $APPS
do
  case "$NAME" in
    'core4')
        echo "Deploy ""$NAME"" Start**********"
        echo ${NAME}
        APP="OmegaCore"
        sh ~/bin/deploy.sh;;
    'ics'|'cms'|'connect'|'ps'|'ips'|'txs'|'tron')
        echo "Deploy ""$NAME"" Start**********"
        echo ${NAME}
        APP="Omega""${NAME^}"
        if [ "$NAME" == "ics" ]; then
            APPLICATION="OmegaIcsApps"
            TARGET_WAR=$APPLICATION-$BRANCH.war
        else
            TARGET_WAR=$APP-$BRANCH.war
        fi
        sh ~/bin/deploy.sh $UPLOAD/$TARGET_WAR;;
    esac
done
# optional input arguments: 3rd argument, default yes, start tomcat
# it is the case on demo that no tomcat start needed, passing no to it
if [ ! -z $3 ]
then
    : # demo env no tomcat start needed
else
    : #
    echo "Restart tomcat No default tomcat auto start setting on this server"
    sleep 2
    $CATALINA start
fi
echo "clean up files older than 30 days under back up folder"
find /home/omega/backups -type f -mtime +30 -exec rm -f {} \;
