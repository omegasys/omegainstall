#!/usr/bin/env bash
set -e

# Ensure the backup directory exists
backup_dir="/home/omega/uploads/backup"
mkdir -p "${backup_dir}"

# Function to calculate the folder (currently hardcoded to 'omega-apps')
calculate_folder() {
	echo "omega-apps"
}

# Function to calculate the app name with appropriate version and extension
calculate_app_name() {
	local appName=$1
	local version=$2
	local extension=$3

	# Only 'core' does not append the version number
	if [[ "${appName}" == "core" ]]; then
		echo "${appName}${extension}"
	else
		echo "${appName}-${version}${extension}"
	fi
}

# Function to calculate the extension based on the app group
calculate_extension() {
	local appName=$1

	# Declare associative arrays for extensions
	declare -A extensions
	extensions=(["core"]=".tar.gz" ["core5"]=".tar.gz")

	# WAR group
	for app in "OmegaAms" "OmegaCms" "OmegaConnect" "OmegaIcsApps" "OmegaIps" "OmegaPs" "OmegaTron" "OmegaTxs"; do
		extensions["${app}"]=".war"
	done

	# JAR group
	for app in "OmegaJob" "OmegaRpt" "OmegaXs" "omega-etl"; do
		extensions["${app}"]=".jar"
	done

	# Return the extension or empty if not found
	echo "${extensions[$appName]}"
}

# Validate input arguments
if [[ -z $1 ]]; then
	echo "ERROR: appName is missing."
	exit 127
fi
if [[ -z $2 ]]; then
	echo "ERROR: version is missing."
	exit 127
fi

# Assign input parameters to variables
appName=$1
version=$2

# Calculate extension, folder, and full app name
extension=$(calculate_extension "${appName}")
folder=$(calculate_folder)
app=$(calculate_app_name "${appName}" "${version}" "${extension}")

# Define source and destination paths
source_file_with_bucket="builds.omegasys.eu/${folder}/${version}/${app}"
full_path_destination="/home/omega/uploads/${app}"

# Backup the existing file if it exists
if [[ -f "${full_path_destination}" ]]; then
	echo "A previous file for version ${version} exists. Backing up the old file."
	mv "${full_path_destination}" "${backup_dir}/${app}.$(date +"%Y%m%d%H%M")"
fi

# Execute the file copy operation
java -jar "/home/omega/OmegaInstall/misc/s3-copy.jar" "${source_file_with_bucket}" "${full_path_destination}"
