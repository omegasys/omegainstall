#!/bin/bash
# Get user from command line
if test -z "$1"
then
        echo "Checking User omega"
        ACC='omega'
else
    ACC=$1
fi

echo "User is $ACC"

if test -z "$2"
then
        echo "Setting Password to !0m3ga2016 for user $ACC"
        PASS='!0m3ga2016'
else
    PASS=${2}
fi

echo "PASS is ${PASS}"


#echo adding user if not already exists
id -u  $ACC &>/dev/null || useradd  $ACC
yes $PASS | passwd $ACC
usermod -aG wheel  $ACC