#!/bin/bash

APPLICATION_HOME="/home/omega/apps/"
APPS=(Cms Ics Core Ps Ips Connect Txs Tron Ams Job)

for NAME in "${APPS[@]}"; do
	APP="Omega""${NAME}"
	case "${NAME}" in
	"Core" | "Job" | "Xs" | "Rpt")
		#Check Application Version
		if [[ -d "$APPLICATION_HOME/$APP" ]]; then
			echo ""
			echo "*************VERSION For*************"
			echo "$NAME"
			#cd /home/omega/apps/OmegaCore
			#echo `cat version.json | awk '{print $2}' | sed 's/,//g;s/\"//g' `
			#awk '{gsub("this.gitVersion=\"",RS)}1' 24.*.chunk.js | sed -sn 2p | sed 's/\\n".*//g'
			cat /home/omega/apps/"$APP"/version.txt
		fi
		;;
	*)
		#Check Application Version
		if [[ -d "$APPLICATION_HOME/$APP" ]]; then
			echo ""
			echo "*************VERSION For*************"
			echo "$NAME"
			cat /home/omega/apps/"$APP"/WEB-INF/classes/version.txt
		fi
		;;
	esac
done
