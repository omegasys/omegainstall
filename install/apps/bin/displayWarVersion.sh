#!/bin/bash
tmp_dir=$(mktemp -d -t ci-XXXXXXXXXX)
#echo $tmp_dir

WARFILE=$1
CURDIR=${PWD}

if [ "$CURDIR" == "/home/omega/uploads" ]; then
	case $1 in
	*/*)
		WARFILE=$1
		;;
	*)
		WARFILE="/home/omega/uploads/${1}"
		;;
	esac
fi
echo "War file is '$WARFILE'"

if ! test -e "$1"; then
	echo "War file '$WARFILE' does not exist. Exiting"
	exit
fi

cd "$tmp_dir"
jar xvf "$WARFILE" >/dev/null
cat ./WEB-INF/classes/version.txt

rm -rf "$tmp_dir"
