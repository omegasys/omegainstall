#!/bin/bash

# For Embedded Tomcat Applications like Jobs, Xs, Rpt, Mars etc.
#
#
#VARIABLES

# check if at least one argument is provided
if [ "$#" -lt 1 ]; then
    echo "Usage: $0 <jar_file> [java_options]"
    echo "It will add heap size, heap dump and Zabbix parameter."
    echo "If the filename of jar is not omegaApps.jar, it will create a symbol link in ~/apps directory so that systemctl can start it up."
    echo "Usage: $0 OmegaXs-4.47.0-SNAPSHOT.jar"
    echo "Usage: $0 omegaRpt-4.47.0-SNAPSHOT.jar --spring.config.location=file:/home/omega/apps/conf/application.yml"
    echo "Usage: $0 omegaJob-4.47.0-SNAPSHOT.jar -Dserver.port=9090"
    echo "Usage: $0 OmegaMars-1.1.1.jar --spring.profiles.active=prod"
    echo ""
    echo "To stop, run > stopJar.sh."
    echo "To redeploy, just run the same script. It will kill the old process."
    echo "To check the retrieve ip is correct or not, run > get_ip.sh"
    exit 1
fi
UPLOAD=/home/omega/uploads
APPS_HOME=/home/omega/apps
JAVA_HOME=/home/omega/java

JAR_FILE=$1

# Remove the first argument (the jar file)
shift

# All the remaining arguments are for the Java command
REMAIN_OPTS="$@"
#Usage
#Xs:  /home/omega/bin/startJar.sh  omegaApps.jar
#Rpt: /home/omega/bin/startJar.sh  omegaApps.jar --spring.config.location=file:/home/omega/apps/conf/application.yml
#Job: /home/omega/bin/startJar.sh  omegaApps.jar -Dserver.port=9090
#Mars: /home/omega/bin/startJar.sh omegaApps.jar --spring.profiles.active=prod


mem_kb=$(grep MemTotal /proc/meminfo | awk '{print $2}')
mem_mb=$((mem_kb / 1024))
echo "Total memory: ${mem_mb} mb"
max_r=0.5
min_r=0.5
max_heap=$(echo $mem_mb $max_r | awk '{printf "%d\n", $1*$2}')
min_heap=$(echo $mem_mb $min_r | awk '{printf "%d\n", $1*$2}')

#
MAX_HEAP="$max_heap"
MIN_HEAP="$min_heap"
#

#
#
ServerIP=$(/home/omega/bin/get_ip.sh)

# Get the full Java version string
full_version=$(java -version 2>&1 | awk -F '"' '/version/ {print $2}')
echo "Full Java version is $full_version"

# Extract the major version number from the full version string
# This works for both "1.8.x_xxx" and "17.x.x" formats
if [[ "$full_version" =~ 1\.8\..* ]]; then
    major_version=8
elif [[ "$full_version" =~ ([0-9]+)\..* ]]; then
    major_version=${BASH_REMATCH[1]}
else
    echo "Error: Could not determine Java version"
    exit 1
fi

echo "Major Java version is $major_version"

# Apply JVM arguments based on the extracted major version number
if [[ "$major_version" -eq "21" ]]; then
    # For Java 21
    JVM_OPTS="-server -Xmx${MAX_HEAP}m -Xms${MIN_HEAP}m -XX:+UseG1GC -Djava.util.Arrays.useLegacyMergeSort=true -Xlog:gc:omegaGC.log"
    echo "Setting JVM args for Java 21: $JVM_OPTS"
elif [[ "$major_version" -eq "17" ]]; then
    # For Java 17 or higher
    JVM_OPTS="-server -Xmx${MAX_HEAP}m -Xms${MIN_HEAP}m -XX:+UseG1GC -Djava.util.Arrays.useLegacyMergeSort=true -Xloggc:omegaGC.log"
    echo "Setting JVM args for Java 17: $JVM_OPTS"
elif [[ "$major_version" -eq "8" ]]; then
    # For Java 8
    JVM_OPTS="-server -Xmx${MAX_HEAP}m -Xms${MIN_HEAP}m -XX:+UseG1GC -Djava.util.Arrays.useLegacyMergeSort=true -XX:+CMSClassUnloadingEnabled -XX:+PrintGCDetails -Xloggc:omegaGC.log"
    echo "Setting JVM args for Java 8: $JVM_OPTS"
else
    echo "Unsupported Java version: $major_version"
    exit 1
fi

ZABBIX="-Djava.rmi.server.hostname=${ServerIP} -Dcom.sun.management.jmxremote.rmi.port=10052 -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=12345 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"

JAVA_OPTS="${JVM_OPTS} ${ZABBIX} "

if [[ -z "$APPS_HOME/$JAR_FILE" ]]; then
	echo "Invalid jar file provided"
	exit 0
fi

FILE_NAME="$APPS_HOME/$JAR_FILE"
echo "Fullpath is $FILE_NAME"

APP_NAME=omegaApps.jar
echo "App name is $APP_NAME"
PIDS=$(ps aux | grep java | grep "$APP_NAME" | awk {'print $2'})

echo "Going to stop previous application"
if [ -z "$PIDS" ]; then
	echo "The application is not running. Skip stopping"
else
	echo " Stopping pid: $PIDS"
	kill -9 "$PIDS"
	echo "Application stopped"
fi

#Get service name
symlink_path="$APPS_HOME/omegaApps.jar"
omegaAppsLink=$(readlink "$symlink_path")
echo "omegaAppsLink is ${omegaAppsLink}"

if [[ $omegaAppsLink == *"OmegaJob"* ]]; then
  EXTRA_OPTS="-Dserver.port=9090"
elif [[ $omegaAppsLink == *"OmegaRpt"* ]]; then
  EXTRA_OPTS="--spring.config.location=file:/home/omega/apps/conf/application.yml"
elif [[ $omegaAppsLink == *"OmegaMars"* ]]; then
  EXTRA_OPTS="--spring.profiles.active=prod"
else
  EXTRA_OPTS=""
fi

echo "EXTRA_OPTS is ${EXTRA_OPTS}"

PID_FILE="$APPS_HOME/omegaApps.jar_pid"
echo "Now going to start application"
echo "java -jar \"${FILE_NAME}\" ${JAVA_OPTS}  ${REMAIN_OPTS} ${EXTRA_OPTS}"
nohup java -jar "$FILE_NAME" "${JAVA_OPTS}"  "${REMAIN_OPTS}" "${EXTRA_OPTS}" &

echo $! >"$PID_FILE" &

echo "Application running with pid $(cat "$PID_FILE")"
