#!/bin/bash
# Add port 8081 into firewall exception
firewall-cmd --zone=public --add-port=8081/tcp --permanent
firewall-cmd --zone=dmz --add-port=8081/tcp --permanent
firewall-cmd --reload