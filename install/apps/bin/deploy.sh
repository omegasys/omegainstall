TOMCATWORK="/home/omega/tomcat/work/*"
TOMCATTMP="/home/omega/tomcat/temp/*"
CATALINA=/home/omega/tomcat/bin/catalina.sh

start_and_stop_tomcat() {
	echo "Stopping tomcat"
	pkill -9 -f tomcat
	echo "Tomcat is killed"

	echo "Removing work and temp directory"
	rm -fr "$TOMCATTMP"
	rm -fr "$TOMCATWORK"
	echo "Should we start Tomcat or not ?"

	tomcat=$(systemctl is-active tomcat.service)
	if [ "$tomcat" == "unknown" ]; then
		echo "Start Tomcat since autostart status is unknown"
		$CATALINA start
	elif [ "$tomcat" == "inactive" ]; then
		echo "Start Tomcat since autostart status is inactive"
		$CATALINA start
	elif [ "$tomcat" == "failed" ]; then
		echo "Start Tomcat since autostart status is failed"
		$CATALINA start
	elif [ "$tomcat" == "activating" ]; then
		echo "Tomcat is starting up, so no need to start it."
	elif [ "$tomcat" == "active" ]; then
		echo "Tomcat will be autostarted, so no need to start it up"
	else
		echo "Unknown status of Tomcat autostart $tomcat"
	fi

	# Now wait for tomcat to come up
	isup=$(ps -ef | grep tomcat)
	echo "Wait if tomcat is not ready"
	while [[ $isup != *"Bootstrap"* ]]; do
		echo -n "."
		sleep 1
		isup=$(ps -ef | grep tomcat)
	done
	echo " Tomcat is up."
	echo "-----------------"
	echo "$isup"
	echo "-----------------"
	echo "Deploy completed."

}

deployWar() {
	echo "Deploy War here"
	# ie OmegaIcs-4.19.0.war OmegaCms-4.19.0.war etc
	if [[ $1 == Omega* ]]; then
		echo "Here is filename only so append the current path and make it full path"
		#cwd=`pwd`
		filename="$cwd/$1"
		echo "Fullpath is $filename"
	else
		filename=$1
	fi
	echo "Deploy war $filename"
	deployWar.sh "$filename"
	echo "come back to original directory"
	cd "$cwd" || exit
}

deployCore() {
	echo "deploy core now $1"
	if [[ $1 == core.tar.gz* ]]; then
		echo "Here is filename only so append the pwd and make it full path"
		filename="$cwd/$1"
		echo "Fullpath is $filename"
	else
		filename=$1
	fi
	echo "deploy core $filename"
	deployCore.sh "$filename"
	cd "$cwd" || exit
	echo "come back to original directory '$cwd'"
}

deployJar() {
	echo "deploy jar file $1"
	if [[ $1 == Omega* ]]; then
		echo "Here is filename only so append the current path and make it full path"
		#cwd=`pwd`
		filename="$cwd/$1"
		echo "Fullpath is $filename"
	else
		filename=$1
	fi
	echo "Deploy jar $filename"
	startJar.sh "$filename"
	echo "come back to original directory"
	cd "$cwd" || exit
}

i=1
j=$#
if [ "$#" -eq 0 ]; then
	echo "Invalid parameter, use -h for help"
	exit 0
fi
cwd=$(pwd)
contains_war=false
while [ $i -le $j ]; do
	if [ "$1" == "-h" ]; then
		echo "usage: deploy.sh"
		echo "It allows deploy war and core. Here are some typical use cases."
		echo "Case 1. Deploy core.  ie $ deploy.sh core.tar.gz"
		echo "Case 2. Deploy 1 war. ie $ deploy.sh OmegaPs-4.19.0.war"
		echo "Case 3. Deploy 2 war. ie $ deploy.sh OmegaPs-4.19.0.war OmegaIps-4.19.0.war"
		echo "Case 4. Deploy 3 war. ie $ deploy.sh OmegaIcs-4.19.0.war OmegaCms-4.19.0.war core.tar.gz"
		echo "Case 5. Deploy all.   ie $ deploy.sh *"
		echo "Case 6. Deploy with wildcard. ie $ deploy.sh *4.19.0* core.tar.gz"
		echo "Case 7. Deploy Job. ie $ deploy.sh  omegaApps.jar -Dserver.port=9090"
		echo "Case 8. Deploy Rpt. ie $ deploy.sh  omegaApps.jar --spring.config.location=file:/home/omega/apps/conf/application.yml"
		echo "Case 9. Deploy Mars. ie $ deploy.sh  omegaApps.jar --spring.profiles.active=prod"
		echo "It will also check if tomcat is autostart and only start up tomcat if it is not."
		echo "It wait for tomcat to come up and also display version."
		exit 0
	fi
	echo "Current Directory is $cwd"
	echo "File - $i: $1"
	i=$((i + 1))
	if [[ $1 == *"core.tar.gz"* ]]; then
		contains_war=true
		deployCore "$1"
	elif [[ $1 == *".jar"* ]]; then
		deployJar "$1" "$2"
	else
		contains_war=true
		deployWar "$1"
	fi
	shift 1
done

echo "Deployment is completed"

if [[ "$contains_war" = true ]]; then
	start_and_stop_tomcat
fi

echo "-----------------"
echo "New versions are:"
displayVersion.sh

echo " Clean up old backups directory that is older than 60 days"
find /home/omega/backups/* -maxdepth 0 -type d -ctime +60 -exec rm -rf {} \;
