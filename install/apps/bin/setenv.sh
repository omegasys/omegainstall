#!/bin/sh
# Grant Rosenthal Feb 2019
#
# ENVARS for Tomcat Omega environment - Core4/3
# System version 4 beta
#
# 1024
# 2048
# 4096
# 8192
# 10240
# 12288
# 16384
# 24576
#
CATALINA_HOME="/home/omega/tomcat"
CATALINA_BASE="/home/omega/tomcat"
JAVA_HOME="/home/omega/java"
#
MAX_HEAP=4096
MIN_HEAP=2048
#
DMP_OPTS="-XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/omega/logs/heapdumps"
JMX_OPTS=""
# Unset JDK_JAVA_OPTIONS to prevent the "Picked up" message
unset JDK_JAVA_OPTIONS
CATALINA_OPTS="${DMP_OPTS} ${JMX_OPTS}"
#
#
IP=$(ip a | grep -A3 eth0 | head -3 | tail -1 | awk '{print $2}' | awk -F '/' '{print $1}')
JVM_OPTS="-server -Xmx${MAX_HEAP}m -Xms${MIN_HEAP}m -XX:+UseG1GC -Djava.util.Arrays.useLegacyMergeSort=true -Xlog:gc:omegaGC.log"
ZABBIX="-Djava.rmi.server.hostname=${IP} -Dcom.sun.management.jmxremote.rmi.port=10052 -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=12345 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"

JAVA_OPTS="${JVM_OPTS} "
#
export JAVA_HOME CATALINA_HOME CATALINA_BASE CATALINA_OPTS JAVA_OPTS