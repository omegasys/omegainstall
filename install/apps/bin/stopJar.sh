#!/bin/bash

# Directory to search
DIR="/home/omega/apps"

# Find files with the _pid.file suffix
find "$DIR" -type f -name "*_pid.file" | while read -r pid_file; do
    # Read the PID from the file
    pid=$(cat "$pid_file")

    # Check if the PID is a number
    if [[ $pid =~ ^[0-9]+$ ]]; then
        # Try to kill the process
        echo "Killing process with PID: $pid from file $pid_file"
        kill -9 "$pid"
    else
        echo "Invalid PID in $pid_file: $pid"
    fi
done