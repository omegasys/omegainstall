#!/bin/bash
echo "Stoping process on app $1"
process=$(ps ax | grep $1 | grep java | awk '{print $1}')
if test -z "$process"
then
        echo 'Process is not running'
else
        kill -9 $process
        echo 'process stopped'
fi
