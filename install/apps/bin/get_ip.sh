#!/bin/sh

#This will return the local ip. Run this directly to confirm if it returns the correct value.

interface=$(route -e | head -3 | tail -1 |  awk '{print $8}')
ip=$(ip a | grep $interface | grep inet | awk '{print $2}' | awk -F '/' '{print $1}')

echo $ip