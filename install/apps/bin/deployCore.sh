# This will deploy core.
# If no parameter, it will get the core.tar.gz from the current
# directory.
# If paramter is provided, it have to be full path.
echo "remove old OmegaCore"
cd /home/omega/apps || exit
rm -rf OmegaCore/
rm core.tar.gz

echo "deploy new OmegaCore"
cd /home/omega/uploads || exit
if [[ $# -ne 1 ]]; then
	cp core.tar.gz /home/omega/apps
else
	cp "$1" /home/omega/apps
fi

cd /home/omega/apps || exit
tar zxvf core.tar.gz

mv dist/ OmegaCore

# cp /home/omega/uploads/betrebels_logo.png /home/omega/apps/OmegaCore/assets/img/omega/

if [[ -e /home/omega/apps/conf/custom_background.jpg ]]; then
    mv /home/omega/apps/OmegaCore/assets/img/omega/backgroundv2.jpg /home/omega/apps/OmegaCore/assets/img/omega/backgroundv2-"$(date +'%Y%m%d%H%M')".jpg
    cp /home/omega/apps/conf/custom_background.jpg /home/omega/apps/OmegaCore/assets/img/omega/backgroundv2.jpg
fi

echo "Done"
