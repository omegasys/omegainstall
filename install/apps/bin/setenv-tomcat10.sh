#!/bin/sh
# Environment configuration for Tomcat

setenv_custom_path="$TOMCAT_DIR/bin/setenv-custom.sh"
CATALINA_HOME="/home/omega/tomcat"
CATALINA_BASE="/home/omega/tomcat"
JAVA_HOME="/home/omega/java"

# Memory settings: allocate 50% of system memory to Tomcat
MAX_HEAP=$(($(grep MemTotal /proc/meminfo | awk '{print $2}') * 60 / 100 / 1024))
MIN_HEAP=$(($(grep MemTotal /proc/meminfo | awk '{print $2}') * 50 / 100 / 1024))

# Heap dump options
DMP_OPTS="-XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/omega/logs/heapdumps"

# Placeholder for any JMX-related options (optional, customize if needed)
JMX_OPTS=""

# Aggregate options for Tomcat
CATALINA_OPTS="$DMP_OPTS $JMX_OPTS"

# Get IP for JMX remote access (optional if JMX is needed)
IP=$(ip -o -4 addr show scope global | awk '{print $4}' | grep -v '^127.' | head -n 1 | cut -d '/' -f 1)

# JVM options
JVM_OPTS="-server -Xmx${MAX_HEAP}m -Xms${MIN_HEAP}m -XX:+UseG1GC -Djava.util.Arrays.useLegacyMergeSort=true -Xlog:gc*:file=/home/omega/logs/omegaGC.log:time,level,tags -Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom"

# JMX configuration (if required for monitoring)
ZABBIX_OPTS="-Djava.rmi.server.hostname=${IP} -Dcom.sun.management.jmxremote.rmi.port=10052 -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=12345 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"


# Setting JAVA_OPTS to include all relevant options
JAVA_OPTS="${JVM_OPTS} ${ZABBIX_OPTS}"

if [ -f "$setenv_custom_path" ]; then
    source "$setenv_custom_path"
fi

# Export necessary environment variables
export JAVA_HOME CATALINA_HOME CATALINA_BASE CATALINA_OPTS JAVA_OPTS