#!/bin/sh
# For Embedded Tomcat Applications like Jobs, Xs, Rpt etc.  Merged into deployJar.sh. Can be delete later.
#
#
mem_kb=$(grep MemTotal /proc/meminfo | awk '{print $2}')
mem_mb=$((mem_kb / 1024))
echo "Total memory: ${mem_mb} mb"
max_r=0.5
min_r=0.5
max_heap=$(echo $mem_mb $max_r | awk '{printf "%d\n", $1*$2}')
min_heap=$(echo $mem_mb $min_r | awk '{printf "%d\n", $1*$2}')


JAVA_HOME="/home/omega/java"
#
MAX_HEAP="$max_s"
MIN_HEAP="$min_s"
#
DMP_OPTS="-XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/home/omega/logs/heapdumps "

#
#
ServerIP=$(ip a | grep -A3 eth0 | head -3 | tail -1 | awk '{print $2}' | awk -F '/' '{print $1}')
#ServerIP=$(ip -4 a | grep -A1 ": en" | tail -1 | awk '{print $2}' | awk -F '/' '{print $1}')
JVM_OPTS="-server -Xmx${MAX_HEAP}m -Xms${MIN_HEAP}m -XX:+UseG1GC -Djava.util.Arrays.useLegacyMergeSort=true -Xlog:gc:omegaGC.log"
ZABBIX="-Djava.rmi.server.hostname=${ServerIP} -Dcom.sun.management.jmxremote.rmi.port=10052 -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=12345 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"

JAVA_OPTS="${JVM_OPTS} ${ZABBIX} ${DMP_OPTS}"
#
export JAVA_HOME JAVA_OPTS