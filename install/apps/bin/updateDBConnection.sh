#!/bin/bash

# you can mannually run this script, and then re-install required application
# if db connection string changed in the future

echo "Please provide database connection string, e.g. 10.0.1.1:1433/omega_demo"
read address
#write db schema into environment
#setup bash_profile
rm ~/.bash_profile
echo 'if [ -f ~/.bashrc ];' >> ~/.bash_profile \
&& echo 'then . ~/.bashrc  ' >> ~/.bash_profile \
&& echo 'fi' >> ~/.bash_profile \
&& echo 'JAVA_HOME=/home/omega/java' >> ~/.bash_profile \
&& echo 'PATH=$PATH:$HOME/.local/bin:$HOME/bin:$JAVA_HOME/bin' >> ~/.bash_profile \
&& echo 'export PATH'>> ~/.bash_profile \
&& echo 'export DBCONNECTION='$address >> ~/.bash_profile && source ~/.bash_profile
source ~/.bash_profile