#!/bin/bash

app=$1
version=$2
port=$3

jar_name="$app-$version.jar"
rempote_path="chronos/$version/$jar_name"
app_path="/home/omega/apps/$app.jar"

echo "$jar_name"
echo "$rempote_path"
echo "$app_path"

if [ "$app" != 'chronos-engine' ] && [ "$app" != 'chronos-cms' ] && [ "$app" != 'chronos-producer' ]; then
	echo 'not one of the known apps: chronos-engine, chronos-cms or chronos-producer'
	exit
fi

if [ -f "$app_path" ]; then
	rm "$app_path"
fi

sh app-stop.sh "$app"

java -jar /home/omega/omegainstall/misc/aws-s3-copy.jar "$rempote_path" "$app_path"

java -jar "$app_path" --spring.profiles.active=prod --spring.config.location=classpath:/,file:/home/omega/apps/conf/properties.yml &
