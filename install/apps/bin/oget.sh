#!/bin/sh

## 
if [ $# -eq 0 ]; then
    echo "Usage : oget https://www.dropbox.com/s/wzw7qvmj5zgctd4/OmegaTxs-4.40.war?dl=0"
    echo "This is like a custom version of wget that help to "
    echo " - ignore  trailing ?dl=0 that come from dropbox link and "
    echo " - before wget, it will backup the old war file with today's timestamp."
    echo "So when you deploy, you can always ie. deployPs.sh /home/omega/upload/OmegaTxs4.40.war"
    exit 1
fi

filename=`echo "$1" | sed -e 's/.*\/Omega/Omega/g' | sed -e 's/.*\/core/core/g' | sed -e 's/?dl=0//g'`
echo "File name is $filename"


backupFilename=$filename.`date "+%Y-%m-%d-%H-%M-%S"`
echo "Backup file name is $backupFilename"


if [ -f ./$filename ]
then
  `mv $filename $backupFilename`
  echo "move $filename to $backupFilename"
fi

filename=`echo "$1" | sed -e 's/?dl=0//g'`
wget $filename
