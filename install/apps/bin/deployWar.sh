#!/usr/bin/env bash
set -e

AMS="Ams"
# shellcheck disable=SC2034
CORE="Core"
ICS="Ics"
CMS="Cms"
PS="Ps"
IPS="Ips"
CONNECT="Connect"
TRON="Tron"
TXS="Txs"
EXCLUSION="Exclusion"
# shellcheck disable=SC2034
UPLOAD="/home/omega/uploads"
APPSHOME=/home/omega/apps
BACKUP="/home/omega/backups"
JAR=/home/omega/java/bin/jar
# shellcheck disable=SC2034
TOMCATWORK="/home/omega/tomcat/work/*"
# shellcheck disable=SC2034
TOMCATTMP="/home/omega/tomcat/temp/*"
# shellcheck disable=SC2034
CATALINA=/home/omega/tomcat/bin/catalina.sh

WARFILE=$1
echo "War file is '$WARFILE'"

# WARFILE="/home/omega/uploads/OmegaPs-4.11.0-SNAPSHOT.war"
#    APP=$(echo $WARFILE| sed -e 's/.*Omega\(.*\).war/\1/'| sed -e 's/-RC[123456789]//'| sed -e 's/-SNAPSHOT//')
#    APP=${APP%-*}

APP=$(echo "$WARFILE" | sed -e 's/.*Omega\(.*\).war/\1/')
echo "$APP"
#   Ps-4.11.0-SNAPSHOT or Ps-4.11.0-M1 or Ps-4.11.0
APP=${APP%-*}
echo "$APP"
#   Ps-4.11.0 or Ps-4.11.0 or Ps
APP=${APP%-*}
# Ps
# the case of OmegaIcsApps-4.70.war
# further remove Apps will get the App name
APP=${APP//'Apps'/}
echo '************App to be deployed is:*******************'
echo "$APP"
# $App - Core, Ics, Cms, Connect, Ps, Ips, Tron, Txs

case "$APP" in
"$AMS" | "$ICS" | "$CMS" | "$PS" | "$IPS" | "$CONNECT" | "$TRON" | "$TXS" | "$EXCLUSION")
	if ! test -e "$1"; then
		echo "War file '$WARFILE' does not exist. Exiting"
		exit
	fi

	echo "Deploy ""$APP"" Start**********"

	APP="Omega""${APP}"
	echo "Backing up ""$APP"

	echo "Backing up"
	cp -rip $APPSHOME/"$APP"/ "$BACKUP/$APP_$(date +%Y_%m_%d_%H:%M)"

	echo "remove old ""$APP"
	cd $APPSHOME
	rm -rf "$APP"/*

	echo "deploy new ""$APP"
	cd $APPSHOME/
	mkdir -p "$APP"
	cd "$APP"
	$JAR -xvf "$WARFILE" >/dev/null
	;;

esac
