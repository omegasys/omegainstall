# Get war file from command line
if test -z "$1"; then
	echo "You must specify a war file to deploy"
	exit
fi

WARFILE=$1
CURDIR=${PWD}
if [ "$CURDIR" == "/home/omega/uploads" ]; then
	case $1 in
	*/*)
		WARFILE=$1
		;;
	*)
		WARFILE="/home/omega/uploads/${1}"
		;;
	esac
fi
echo "War file is '$WARFILE'"

if ! test -e "$1"; then
	echo "War file '$WARFILE' does not exist. Exiting"
	exit
fi

echo "Backing up"
cp -r /home/omega/apps/OmegaMars/ "/home/omega/backups/OmegaMars_$(date +%Y_%m_%d_%H:%M)"

echo "Stopping tomcat"
pid=$(ps -ef | grep tomcat | grep java | awk '{ print $2 }')
if [ "${pid}" -gt 0 ]; then
	kill -9 "${pid}"
fi

echo "remove old OmegaMars"
cd /home/omega/apps || exit
rm -rf OmegaMars/*

echo "deploy new OmegaMars"
cd /home/omega/apps/OmegaMars || exit
/home/omega/java/bin/jar -xvf "$WARFILE"

echo "Removing work and temp dir"
rm -fr /home/omega/tomcat/work/*
rm -fr /home/omega/tomcat/temp/*

pid=$(ps -ef | grep tomcat | grep java | awk '{ print $2 }')
if [ -z "${pid}" ]; then
	/home/omega/tomcat/bin/catalina.sh start
fi

echo "clean up files older than 30 days under back up folder"
find /home/omega/backups -type f -mtime +30 -exec rm -f {} \;
