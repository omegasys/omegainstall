#!/bin/bash
#VARIABLES
JAR_FILE="$1"
UPLOAD=/home/omega/uploads
APPS_HOME=/home/omega/apps

if [[ -z "$JAR_FILE" ]]; then
	echo "Invalid jar file provided"
	exit 0
fi

FULL_FILE_NAME=""
# Check if the parameter is not "omegaApps.jar"
if [ "$1" != "omegaApps.jar" ]; then

    if [[ $1 == Omega* ]]; then
      FULL_FILE_NAME="${UPLOAD}/$1"
    else
      FULL_FILE_NAME=$1
    fi

    echo "full file path is ${FULL_FILE_NAME}"
    # Check if the file exists
    unlink /home/omega/apps/omegaApps.jar 2>/dev/null
    ln -s  "$FULL_FILE_NAME" "$APPS_HOME/omegaApps.jar"
else
    echo "Called from systemctl"
fi

JAR_FILE="omegaApps.jar"

FILE_NAME="$UPLOAD/$JAR_FILE"
echo "Fullpath is $FILE_NAME"

APP_NAME=${JAR_FILE/-*/}
echo "App name is $APP_NAME"
PIDS=$(ps aux | grep java | grep "$APP_NAME" | awk {'print $2'})

echo "Going to stop previous application"
if [ -z "$PIDS" ]; then
	echo "The application is not running. Skip stopping"
else
	echo " Stopping pid: $PIDS"
	kill -9 "$PIDS"
	echo "Application stopped"
fi


echo "Going to start application"
echo "./startJar.sh ${JAR_FILE}"
startJar.sh $JAR_FILE
