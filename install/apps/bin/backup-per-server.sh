#!/usr/bin/env bash
#set -e
TODAYTIME="$(date +%F)-$(date +%T)"
RED='\033[0;31m'
GREEN='\033[0;32m'
APPLICATION_HOME="/home/omega/apps/"
UPLOAD="/home/omega/uploads"
BACKUP="/home/omega/uploads/backup"

echo $PWD
echo $USER

if test -z "$1"
then
        echo "You must specify release version to backup"
        exit
fi
# get release version from first input argument
BRANCH=$1
if test -z "$2"
then
        echo "You must specify applications to backup"
        exit
fi
APPS=$2
echo "Applications to be deployed are:"
echo $APPS

echo "create back up folder if not exist"
mkdir -p "$BACKUP"

for NAME in $APPS
do
  case "$NAME" in
    'core4')
        echo "Backup ""$NAME"" Start**********"
        echo ${NAME}
        APP="OmegaCore"
        mv $UPLOAD/core.tar.gz $BACKUP/core.tar.gz_$TODAYTIME;;

    'ics'|'cms'|'connect'|'ps'|'ips'|'txs'|'tron')
        echo "Backup ""$NAME"" Start**********"
        echo ${NAME}
        APP="Omega""${NAME^}"
        if [ "$NAME" == "ics" ]; then
            APPLICATION="OmegaIcsApps"
            TARGET_WAR=$APPLICATION-$BRANCH.war
        else
            TARGET_WAR=$APP-$BRANCH.war
        fi
        echo "****************Backup****************"
        mv $UPLOAD/$TARGET_WAR $BACKUP/$TARGET_WAR"-"$TODAYTIME;;
    esac
done
